#!/usr/bin/python
# Process binary diagnostic report. 
import sys
import math

input_file=open(sys.argv[1], "r")

counts = []
total_count = 0

for input_value in input_file:
    input_value = input_value.strip()
    print "Found " + input_value + " which = " + str(int(input_value, 2))
    # stub out empty counts if we don't already have them
    if len(counts) != len(input_value):
        for char in input_value:
            counts.append(0)
            
    for i in range(0, len(input_value)):
      counts[i] += int(input_value[i])

    total_count += 1

print counts

gamma = ""
epsilon = ""
for value in counts:
    if value > (total_count / 2):
        gamma += "1"
        epsilon += "0"
    else:
        gamma += "0"
        epsilon += "1"

print "Gamma: " + gamma
print "Epsilon: " + epsilon
print "Result: " + str(int(gamma, 2) * int(epsilon, 2))

