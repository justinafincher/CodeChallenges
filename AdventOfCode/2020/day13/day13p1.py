#!/usr/bin/python
# Use bus schedule to determine when you can get to the airport  
import sys
import math

# First line of input is when you want to try to leave
# Second line is list of running busses (ignore x values)

input_file=open(sys.argv[1], "r")
departure_time = int(input_file.readline().strip())
temp_bus_schedule = input_file.readline().strip().split(",")

bus_schedule = []
for bus in temp_bus_schedule:
    if bus != "x":
        bus_schedule.append(int(bus))

print departure_time
print bus_schedule

test_time = departure_time
target_bus = -1

while 1:
    for bus in bus_schedule:
        if test_time % bus == 0:
            print "Can ride bus " + str(bus)
            target_bus = bus
            break

    if target_bus != -1:
        break

    test_time += 1

print str((test_time - departure_time) * target_bus)
