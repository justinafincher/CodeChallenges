#!/usr/bin/python3
# camel cards 
import sys
import math

card_replacements = {
    "A" : "F",
    "K" : "E",
    "Q" : "D",
    "J" : "C",
    "T" : "B"
}

def calculate_hand_rank(hand):
    if len(hand[0]) != 5:
        print("Error, incorrect hand length")
        return -1

    frequencies = {}

    #print(f"Caculating hand rank for card {hand[0]}")
    for card in hand[0]:
        if card in frequencies.keys():
            frequencies[card] += 1
        else:
            frequencies[card] = 1

    card_counts = list(frequencies.values())

    score = 0

    if 5 in card_counts:
        score = 6
    elif 4 in card_counts:
        score = 5
    elif 3 in card_counts and 2 in card_counts:
        score = 4
    elif 3 in card_counts:
        score = 3
    elif card_counts.count(2) == 2:
        score = 2
    elif 2 in card_counts:
        score = 1

    hand.append(str(score))

    return hand


def add_hand(handlist, hand_with_bid):
    
    hand_with_bid_and_rank = calculate_hand_rank(hand_with_bid)
    
    if len(handlist) == 0:
        handlist.append(hand_with_bid)
        return

    for i in range(0, len(handlist)):
        #print("Evaluating whether " + " ".join(hand_with_bid) + " should be inserted in front of "+ " ".join(handlist[i]))
        if hand_with_bid[2] > handlist[i][2]:
            #print("Hand is higher, inserting")
            handlist.insert(i, hand_with_bid)
            return
        elif hand_with_bid[2] == handlist[i][2]:
            #print("hand same rank, checking lexographically")
            if hand_with_bid[0] > handlist[i][0]:
                #print("hand same rank, higher lexographically")
                handlist.insert(i, hand_with_bid)
                return
    #print("hand lower than all currently in handlist")
    handlist.append(hand_with_bid)
    return


input_file=open(sys.argv[1], "r")

sorted_hands = []

for input_value in input_file:
    input_value = input_value.strip()

    # replace face cards with new values that sort lexigraphically
    #print(input_value)
    for replacement in card_replacements.keys():
        input_value = input_value.replace(replacement, card_replacements[replacement])
    #print(input_value)
    input_values = input_value.split()

    #print(calculate_hand_rank(input_values))

    #print(sorted_hands)    
    add_hand(sorted_hands, input_values)
    #print("After adding hand")
    #print(sorted_hands)

sorted_hands.reverse()

#print(sorted_hands)
#print(len(sorted_hands))
total = 0

for i in range(0, len(sorted_hands)):
    total += (i+1)*int(sorted_hands[i][1])

print(total)
