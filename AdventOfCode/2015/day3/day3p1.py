#!/usr/bin/python3
# given directions, tell how many houses are visited
import sys
import math

input_file=open(sys.argv[1], "r")

#houses_visited = {}

for input_value in input_file:
    housing_coord_x = 0
    housing_coord_y = 0
    houses_visited = {"0,0": 1}
    input_value = input_value.strip()
    for move in input_value:
        #print(f"move: {move}")
        if move == "<":
            housing_coord_x -= 1
        elif move == ">":
            housing_coord_x += 1
        elif move == "v":
            housing_coord_y -= 1
        elif move == "^":
            housing_coord_y += 1
        else:
            print(f"INVALID MOVE {move}")

        #print(f"New coord: {housing_coord_x}, {housing_coord_y}")
        coord = str(housing_coord_x) + "," + str(housing_coord_y)
        if coord not in houses_visited.keys():
            houses_visited[coord] = 1

    #print(houses_visited)
    print(f"Visited {sum(houses_visited.values())} houses")


