#!/usr/bin/python
# Process binary diagnostic report. 
import sys
import math

input_file=open(sys.argv[1], "r")

total_input = []

# For each column, filter rows, then continue
def filter_rows_wrapper(input_values, oxygen_or_co2):
    print input_values
    for i in range(0, len(input_values[0])):
        input_values = filter_rows(input_values, i, oxygen_or_co2)
        print input_values
        if len(input_values) == 1:
            break
    
    return input_values

def filter_rows(input_values, index, oxygen_or_co2):
    # For indicated column, calculate most 
    total_count = 0
    count_at_index = 0
    # The value to look for at designated index to decide whether to keep the row
    filter_value = 0

    for input_value in input_values:
        count_at_index += int(input_value[index])
        total_count += 1
    
    print "For index " + str(index) + " calculated count of " + str(count_at_index) + " and total count of " + str(total_count)
    # if count > half the total, 1s are more common
    if oxygen_or_co2 == "oxygen":
        if count_at_index >= (total_count / 2.0):
            filter_value = 1
        else:
            filter_value = 0
    else:
        if count_at_index >= (total_count / 2.0):
            filter_value = 0
        else:
            filter_value = 1
    
    print "filter value = " + str(filter_value)
    new_values = []
    
    for input_value in input_values:
        if input_value[index] == str(filter_value):
            new_values.append(input_value)
    
    return new_values

# we'll do a couple passes, so just store the input
for input_value in input_file:
    input_value = input_value.strip()
    total_input.append(input_value)

print "Calculating oxygen values"
oxygen_values = filter_rows_wrapper(total_input, "oxygen")
print "Calculating co2 values"
co2_values = filter_rows_wrapper(total_input, "co2")
print "Final values"
print oxygen_values
print co2_values
print "Result: " + str(int(oxygen_values[0], 2) * int(co2_values[0], 2))
