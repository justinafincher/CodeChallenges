#!/usr/bin/python
# process polymer insertions 
import sys
import math
import numpy

input_file=open(sys.argv[1], "r")

def do_insertions(pair_counts, instructions):
    new_pair_counts = {}
    
    for pair in pair_counts.keys():
        if pair in instructions.keys():
            inserted_character = instructions[pair]
            if pair[0]+inserted_character in new_pair_counts.keys():
                new_pair_counts[pair[0]+inserted_character] += pair_counts[pair]
            else:
                new_pair_counts[pair[0]+inserted_character] = pair_counts[pair]

            if inserted_character+pair[1] in new_pair_counts.keys():
                new_pair_counts[inserted_character+pair[1]] += pair_counts[pair]
            else:
                new_pair_counts[inserted_character+pair[1]] = pair_counts[pair]
    
    return new_pair_counts

def calculate_score(pair_counts):
    character_counts = {}
    for pair in pair_counts:
        if pair[0] in character_counts.keys():
            character_counts[pair[0]] += pair_counts[pair]
        else:
            character_counts[pair[0]] = pair_counts[pair]

        if pair[1] in character_counts.keys():
            character_counts[pair[1]] += pair_counts[pair]
        else:
            character_counts[pair[1]] = pair_counts[pair]

    print character_counts
    return ((max(character_counts.values()) - min(character_counts.values()))+1)/2

pair_counts = {}
instructions = {}

for input_value in input_file:
    input_value = input_value.strip()
    if(len(input_value) == 0):
        next
    elif "->" not in input_value:
        for i in range(1, len(input_value)):
            if input_value[i-1:i+1] in pair_counts.keys():
                pair_counts[input_value[i-1:i+1]] += 1
            else:
                pair_counts[input_value[i-1:i+1]] = 1
    else:
        instructions[input_value.split(" -> ")[0]] = input_value.split(" -> ")[1]

print pair_counts

iterations = 40

for i in range(0, iterations):
    new_pair_counts = do_insertions(pair_counts, instructions)
    pair_counts = new_pair_counts.copy()

print pair_counts
print calculate_score(pair_counts)
