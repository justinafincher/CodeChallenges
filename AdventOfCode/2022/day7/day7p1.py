#!/usr/bin/python3
# Using bash-like output, find directory sizes 
import sys
import math
import re

directory_size_limit = 100000

def calculate_dir_sizes(file_list):
    directories = {}
    for file_name in file_list.keys():
        print(file_name)
        parent_directories = file_name.split("/")[:-1]
        print("processing directories")
        print(parent_directories)
        for i in range(1, len(parent_directories)+1):
            directory = "/".join(parent_directories[1:i])
            print("Processing directory: "+directory)
            if directory == "":
                continue
            if directory in directories.keys():
                directories[directory] += file_list[file_name]
            else:
                directories[directory] = file_list[file_name]

    return directories


input_file=open(sys.argv[1], "r")

current_directory = "/"

# May be able to just track files with full paths, then use that to calculate directory sizes
files = {}

# Track when we do an ls
reading_files = False

for input_value in input_file:
    input_values = input_value.strip().split(" ")
    #print("NEW LINE ================")
    #print(input_values)
    #print(current_directory)

    if reading_files and input_values[0] == "$":
        reading_files = False

    # update current directory
    if input_values[1] == "cd":
        #print("Changing to directory "+input_values[2])
        if input_values[2] == "..":
            current_directory = "/".join(current_directory.split("/")[:-2])+"/"
        elif input_values[2] == "/":
            current_directory = "/"
        else:
            current_directory += (input_values[2]+"/")
    elif input_values[1] == "ls":
        reading_files = True
    else:
        if input_values[0] != "dir":
            files[current_directory+input_values[1]] = int(input_values[0])

print(files)
directory_list = calculate_dir_sizes(files)
print(directory_list)

total = 0
for directory in directory_list.keys():
    if directory_list[directory] <= directory_size_limit:
        total += directory_list[directory]

print(total)
