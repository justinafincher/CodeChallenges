#!/usr/bin/python3
# Word search, find all XMAS 
import sys
import re

input_file=open(sys.argv[1], "r")

xmas_found_count = 0

original_word_search = []

def find_all_x_mas(word_search):
    found_counter = 0

    x_letters = ["M", "S"]

    for i in range(2,len(word_search)):
        for j in range(2,len(word_search[0])):
            #print(f"Evaluating...{i},{j}")
            
            # Check for middle "A"
            if word_search[i-1][j-1] != "A":
                continue
    
            if word_search[i-2][j-2] not in x_letters or word_search[i][j] not in x_letters or word_search[i-2][j] not in x_letters or word_search[i][j-2] not in x_letters:
                continue
            
            if word_search[i-2][j-2] == "M" and word_search[i][j] != "S":
                continue
            if word_search[i-2][j-2] == "S" and word_search[i][j] != "M":
                continue
            if word_search[i][j] == "M" and word_search[i-2][j-2] != "S":
                continue
            if word_search[i][j] == "S" and word_search[i-2][j-2] != "M":
                continue
            if word_search[i][j-2] == "M" and word_search[i-2][j] != "S":
                continue
            if word_search[i][j-2] == "S" and word_search[i-2][j] != "M":
                continue
            if word_search[i-2][j] == "M" and word_search[i][j-2] != "S":
                continue
            if word_search[i-2][j] == "S" and word_search[i][j-2] != "M":
                continue

            found_counter += 1

            print(f"{word_search[i-2][j-2]} {word_search[i-2][j-1]} {word_search[i-2][j]}")
            print(f"{word_search[i-1][j-2]} {word_search[i-1][j-1]} {word_search[i-1][j]}")
            print(f"{word_search[i][j-2]} {word_search[i][j-1]} {word_search[i][j]}")
            print(f"Found xmas with bottom right at {i},{j}")

    return found_counter


for input_value in input_file:
    input_value = input_value.strip()

    row_values = []
    
    for i in range(0,len(input_value)):
        row_values.append(input_value[i])

    original_word_search.append(row_values)

print(find_all_x_mas(original_word_search))
