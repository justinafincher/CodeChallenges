#!/usr/bin/python3
# parens calculate ending floor 
import sys
import math

input_file=open(sys.argv[1], "r")

floor = 0


for input_value in input_file:
    input_value = input_value.strip()
    floor += input_value.count('(')
    floor -= input_value.count(')')

print(f"Floor: {floor}")
