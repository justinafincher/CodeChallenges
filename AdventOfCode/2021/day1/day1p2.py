#!/usr/bin/python
# Read file with int on each line. Calculate number of times value increases across sliding window of size 3 
import sys
import math

input_file=open(sys.argv[1], "r")

values = []
previous_value = 0
number_of_increases = -1 # janky to skip first "increase"

for input_value in input_file:
    values.append(int(input_value))


for i in range(0, len(values)-2):
    window_value = values[i] + values[i+1] + values[i+2]
    if window_value > previous_value:
        number_of_increases += 1
    previous_value = window_value

print "Found " + str(number_of_increases) + " increases with windowing"
