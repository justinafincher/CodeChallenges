#!/usr/bin/python
# find path with lowest risk
import sys
import math
import numpy

input_file=open(sys.argv[1], "r")

cave_costs = []

for input_value in input_file:
    input_value = input_value.strip()
    cave_costs.append(input_value)

print cave_costs

lowest_cost = 100000

rows = len(cave_costs)
columns = len(cave_costs[0])

risks = numpy.full((rows, columns),0)

for i in range(0, rows):
    for j in range(0, columns):
        if i == j == 0:
            next
        else: # check above and left for minimum to add to
            if i-1 < 0:
                risks[i,j] = risks[i,j-1] + int(cave_costs[i][j])
            elif j-1 < 0:
                risks[i,j] = risks[i-1,j] + int(cave_costs[i][j])
            else:
                risks[i,j] = int(cave_costs[i][j]) + min(risks[i-1,j], risks[i,j-1])

print risks
