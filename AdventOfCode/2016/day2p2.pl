use strict;
use warnings;
use Data::Dumper;

my $filename = 'day2input';
#$filename = 'day2testinput';

my $xcoord = -2;
my $ycoord = 0;
my %lookup = (
   "0,2" => 1,
   "-1,1" => 2,
   "0,1" => 3,
   "1,1" => 4,
   "-2,0" => 5,
   "-1,0" => 6,
   "0,0" => 7,
   "1,0" => 8,
   "2,0" => 9,
   "-1,-1" => "A",
   "0,-1" => "B",
   "1,-1" => "C",
   "0,-2" => "D"
);

my $passcode = "";

open(my $fh, '<:encoding(UTF-8)', $filename)
  or die "Could not open file '$filename' $!";
   
while (my $row = <$fh>) {
   chomp $row;
   my $new_coord;
   my @directions = split('',$row);
   foreach my $direction (@directions) {
     
      print "At $xcoord,$ycoord and moving $direction\n"; 
      if($direction eq "U"){
         $new_coord = $ycoord+1;
         if(defined($lookup{"$xcoord,$new_coord"})){
            $ycoord++;
         }
      }
      elsif($direction eq "D"){
         $new_coord = $ycoord-1;
         if(defined($lookup{"$xcoord,$new_coord"})){
            $ycoord--;
         }
      }
      elsif($direction eq "L"){
         $new_coord = $xcoord-1;
         if(defined($lookup{"$new_coord,$ycoord"})){
            $xcoord--;
         }
      }
      elsif($direction eq "R"){
         $new_coord = $xcoord+1;
         if(defined($lookup{"$new_coord,$ycoord"})){
            $xcoord++;
         }
      }
      #else{
      #   print "EXCEPTION: Direction unknown ($direction)\n";
      #   exit;
      #}
   }

   $passcode .= $lookup{"$xcoord,$ycoord"};
}

print "Passcode is $passcode\n";

