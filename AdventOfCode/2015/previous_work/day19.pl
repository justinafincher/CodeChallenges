
use strict;
use warnings;
use Scalar::Util qw(looks_like_number);
use Data::Dumper;

my $filename = $ARGV[0];
my %transitions;

open IN, "$filename" or die "could not open $filename";

#my $character;
#my $total = 0;
#my $temp = "";
#my $othertemp = "";
#my $lookingforred;

my $get_output = 0;
my $output_string;

while (my $row = <IN>) {
   chomp $row;
   if(length($row) == 0){
      $get_output = 1;
      next;
   }
   unless($get_output){
      my @values = split(' ',$row);
      $transitions{$values[0]} = $values[2];
      next;
   }

   $output_string = $row;
}

my %result_strings;

foreach my $string (keys %transitions){
   my $result = index($output_string, $string, $offset);

   while ($result != -1) {
      print "Found $string at $result\n";

      $offset = $result + 1;
      $result = index($string, $char, $offset);
   }
}

print Dumper(%transitions);

print "Goal: $output_string\n";

