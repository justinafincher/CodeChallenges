#!/usr/bin/python3
# process engine schematic for gondola 
import sys
import math
from collections import namedtuple

non_symbols = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "."]

Point = namedtuple("Point", ["x", "y"])

gears = {}

# Take an array of points and an array of symbol locations and determine if there's any adjacency
def update_gears(test_points, number):
    for point in test_points:
        for gear in gears:
            gear_coords = gear.split(",")
            if abs(point[0]-int(gear_coords[0])) < 2 and abs(point[1]-int(gear_coords[1])) < 2:
                if(gear == "25,136"):
                    print("SPECIAL CASE")
                    print(test_points)
                    print(number)
                gears[gear].append(number)
                
                return

input_file=open(sys.argv[1], "r")

gear_ratio_sum = 0

schematic = []

for input_value in input_file:
    input_value = input_value.strip()
    temp_values = []
    for character in input_value:
        temp_values.append(character)
    schematic.append(temp_values)

# Note locations of symbols
for i in range(0,len(schematic)):
    for j in range(0,len(schematic[i])):
        if schematic[i][j] == "*":
            gears[str(i) + "," + str(j)] = []

# Find numbers then check if adjacent to symbol
building_number = False

temp_num = ""
temp_coords = []

for i in range(0,len(schematic)):
    for j in range(0,len(schematic[i])):
        #print("Evaluating char: "+schematic[i][j])
        if schematic[i][j].isnumeric() and building_number:
            #print("continuing started number")
            temp_num += schematic[i][j]
            temp_coords.append(Point(i,j))
        elif schematic[i][j].isnumeric():
            #print("starting new number")
            temp_num = schematic[i][j]
            building_number = True
            temp_coords.append(Point(i,j))
        elif not schematic[i][j].isnumeric() and building_number:
            print("Found number "+temp_num)
            print(temp_coords)
            # test if valid
            update_gears(temp_coords, temp_num)
            temp_coords = []
            temp_num = ""
            building_number = False
        else:
            continue

    if building_number:
        # also test valid here
        #print("found number "+temp_num+" at end of line")
        update_gears(temp_coords, temp_num)
    building_number = False
    temp_num = ""
    temp_coords = []

print(gears)
print("Found "+str(len(gears))+" total possible gears")

valid_gear_counter = 0
for gear in gears:
    if len(gears[gear]) == 2:
        print("Found gear "+gear+" adjacent to " + gears[gear][0] + " and " + gears[gear][1] + " with gear ratio "+str(int(gears[gear][0])*int(gears[gear][1])))
        gear_ratio_sum += (int(gears[gear][0])*int(gears[gear][1]))
        valid_gear_counter += 1
    else:
        print(gear + ": not valid gear")
        print(gears[gear])

print("Found " + str(valid_gear_counter) + " valid gears")
print(gear_ratio_sum)

