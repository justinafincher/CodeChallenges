#!/usr/bin/python3
# process seed almanac 
import sys
import math

input_file=open(sys.argv[1], "r")

def lookup_mapping(value, mappings):
    #print(f"Evaluating mapping for {value}")
    for mapping in mappings:
        mapping_values = mapping.split()
        if int(mapping_values[1]) <= int(value) <= (int(mapping_values[1]) + int(mapping_values[2])):
            #print(f"found mapping for {value} using {mapping}")
            return int(value) + (int(mapping_values[0])-int(mapping_values[1]))
    
    #print(f"No mapping found for {value} so returning as-is")
    return value


# this is so janky, but I'm tired
seeds = []
seed_to_soil = []
soil_to_fertilizer = []
fertilizer_to_water = []
water_to_light = []
light_to_temperature = []
temperature_to_humidity = []
humidity_to_location = []

process_seed_to_soil = False
process_soil_to_fertilizer = False
process_fertilizer_to_water = False
process_water_to_light = False
process_light_to_temperature = False
process_temperature_to_humidity = False
process_humidity_to_location = False


for input_value in input_file:
    input_value = input_value.strip()
    if input_value.startswith("seeds"):
        seed_values = input_value.split(":")[1].split()
        for i in range(0, len(seed_values), 2):
            seeds.append(seed_values[i]+" "+seed_values[i+1])
    elif len(input_value) < 2:
        process_seed_to_soil = False
        process_soil_to_fertilizer = False
        process_fertilizer_to_water = False
        process_water_to_light = False
        process_light_to_temperature = False
        process_temperature_to_humidity = False
        process_humidity_to_location = False
    elif input_value.startswith("seed-to-soil"):
        process_seed_to_soil = True
    elif input_value.startswith("soil-to-fertilizer"):
        process_soil_to_fertilizer = True
    elif input_value.startswith("fertilizer-to-water"):
        process_fertilizer_to_water = True
    elif input_value.startswith("water-to-light"):
        process_water_to_light = True
    elif input_value.startswith("light-to-temperature"):
        process_light_to_temperature = True
    elif input_value.startswith("temperature-to-humidity"):
        process_temperature_to_humidity = True
    elif input_value.startswith("humidity-to-location"):
        process_humidity_to_location = True
    elif process_seed_to_soil:
        seed_to_soil.append(input_value)
    elif process_soil_to_fertilizer:
        soil_to_fertilizer.append(input_value)
    elif process_fertilizer_to_water:
        fertilizer_to_water.append(input_value)
    elif process_water_to_light:
        water_to_light.append(input_value)
    elif process_light_to_temperature:
        light_to_temperature.append(input_value)
    elif process_temperature_to_humidity:
        temperature_to_humidity.append(input_value)
    elif process_humidity_to_location:
        humidity_to_location.append(input_value)
    else:
        print("Unexpected input: "+input_value)

print("seeds")
print(seeds)
#exit()
#print("seed_to_soil")
#print(seed_to_soil)
#print("soil_to_fertilizer")
#print(soil_to_fertilizer)
#print("fertilizer_to_water")
#print(fertilizer_to_water)
#print("water_to_light")
#print(water_to_light)
#print("light_to_temperature")
#print(light_to_temperature)
#print("temperature_to_humidiy")
#print(temperature_to_humidity)
#print("humidity_to_location")
#print(humidity_to_location)

# can we process backwards?
# for a list of possible paths backwards, we'd merge temperature_to_humidity unmapped outputs and humidity_to_locations outputs
#for mapping in humidity_to_location:
#    mapping_values = mapping.split()

min_location = 1000000000000000

for seed_range in seeds:
    print(f"Processing seed range {seed_range}")
    seed_values = seed_range.split()    
    for i in range(0, int(seed_values[1])):
        seed = int(seed_values[0])+i 
        #print(f"seed: "+str(seed))
        soil = lookup_mapping(seed, seed_to_soil) 
        #print(f"soil: "+str(soil))
        fertilizer = lookup_mapping(soil, soil_to_fertilizer)
        #print(f"fertilizer: "+str(fertilizer))
        water = lookup_mapping(fertilizer, fertilizer_to_water)
        #print(f"water: "+str(water))
        light = lookup_mapping(water, water_to_light)
        #print(f"light: "+str(light))
        temperature = lookup_mapping(light, light_to_temperature)
        #print(f"temperature: "+str(temperature))
        humidity = lookup_mapping(temperature, temperature_to_humidity)
        #print(f"humidity: "+str(humidity))
        location = lookup_mapping(humidity, humidity_to_location)
        #print(f"location: "+str(location))

        if location < min_location:
            min_location = location

#print(locations)

print(min_location)

