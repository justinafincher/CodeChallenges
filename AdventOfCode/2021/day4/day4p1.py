#!/usr/bin/python
# Play bingo
import sys
import math
import numpy

class bingo_board:

    # Assumes board_data is passed a 5 arrays of strings with 5 space separated characters
    def __init__(self, board_data, identifier):
        self.board = numpy.zeros((5, 5)) 
        for i in range(0, 5):
            temp_line = board_data[i].split()
            for j in range(0, 5):
                self.board[i, j] = int(temp_line[j])
        self.identifier = identifier

    # Mark a value on the board as 0 once called
    def mark_value(self, value):
        for i in range(0, 5):
            for j in range(0, 5):
                if self.board[i, j] == value:
                    self.board[i, j] = -1

    # Check if the board has a winner
    def check_board(self):
        column_sums = self.board.sum(axis=0)
        row_sums = self.board.sum(axis=1)
        if -5 in row_sums or -5 in column_sums:
            return "winner"
        else:
            return "no_winner"

    def show_board(self):
        print self.identifier
        print self.board

    def calculate_score(self, called_number):
        total_sum = 0
        for i in range(0, 5):
            for j in range(0, 5):
                if self.board[i, j] != -1:
                    total_sum += self.board[i, j]

        return int(total_sum * called_number)


input_file=open(sys.argv[1], "r")

read_sequence = 0
called_numbers = []
bingo_boards = []
temp_board = []
board_number = 0
for input_value in input_file:
    input_value = input_value.strip()
    if read_sequence == 0:
        for number in input_value.split(","):
            called_numbers.append(int(number))
        read_sequence = 1
    elif len(temp_board) > 0 and len(input_value) == 0:
        bingo_boards.append(bingo_board(temp_board, board_number))
        temp_board = []
        board_number += 1
    elif len(input_value) != 0:
        temp_board.append(input_value)

if len(temp_board) > 0:
    bingo_boards.append(bingo_board(temp_board, board_number))

# Loop through called numbers and mark boards
result = "no_winner"
score = 0
for called_number in called_numbers:
    for board in bingo_boards:
        board.mark_value(called_number)
        result = board.check_board()
        if result == "winner":
            score = board.calculate_score(called_number)
            break
    
    if result == "winner":
        break

print "Final result: " + str(score) 

