use strict;
use warnings;
use Data::Dumper;

my $filename = 'day7input';
#$filename = 'day7testinput';

open(my $fh, '<:encoding(UTF-8)', $filename)
  or die "Could not open file '$filename' $!";

my @strings;
my $supported_ips = 0;

MAIN_LOOP:
while (my $row = <$fh>) {
   chomp $row;
   print "$row\n";
   # store strings as within and outside brackets
   my @values = split('',$row);
   my @inside_brackets;
   my @outside_brackets;

   my $currently_inside = 0;
   my $temp_string;
   foreach my $letter (@values) {
      if($letter eq '['){
         $currently_inside = 1;
         if(defined($temp_string)){
            push(@outside_brackets,$temp_string);
            $temp_string = undef;
         }
         next;
      }
      elsif($letter eq ']'){
         $currently_inside = 0;
         if(defined($temp_string)){
            push(@inside_brackets,$temp_string);
            $temp_string = undef;
         }
         next;
      }
      
      $temp_string .= $letter;
   }
   if($currently_inside && defined($temp_string)){
      push(@inside_brackets,$temp_string);
   }
   elsif(defined($temp_string)){
      push(@outside_brackets,$temp_string);
   }


   print "Strings inside brackets:".Dumper(@inside_brackets);
   print "Strings outside brackets:".Dumper(@outside_brackets);
   
   foreach my $string (@inside_brackets) {
      my $res = find_abba($string);
      if($res){
         print "Found ABBA in $string so IP is invalid\n";
         next MAIN_LOOP;
      }
      #else{
      #   print "ABBA not found in $string\n";
      #}
   }
   
   foreach my $string (@outside_brackets){
      my $res = find_abba($string);
      if($res){
         print "Found ABBA in $string so IP is valid\n";
         $supported_ips++;
         next MAIN_LOOP;
      }
   }

   print "No ABBAs found so IP is invalid\n";
}

print "Found $supported_ips valid IP addresses\n";
sub find_abba {
   my $string = shift;

   my @values = split('',$string);

   for(my $i = 3; $i < @values; $i++){
      my $temp1 = $values[$i-3].$values[$i-2];
      my $temp2 = $values[$i].$values[$i-1];

      if($temp1 eq $temp2 && $values[$i] ne $values [$i-1]){
         return 1;
      }
   }

   return 0;
}
