#!/usr/bin/python
# Read file with signed int on each line. Calculate sum.

input_file=open("day1input.txt", "r")

sum = 0

for line in input_file:
    sum += int(line)

print sum
