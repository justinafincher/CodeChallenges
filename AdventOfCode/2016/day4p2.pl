use strict;
use warnings;
use Data::Dumper;

my $filename = 'day4input';
#$filename = 'day4testinput2';

open(my $fh, '<:encoding(UTF-8)', $filename)
  or die "Could not open file '$filename' $!";

my $sector_id_sum = 0;

while (my $row = <$fh>) {
   chomp $row;
   #print "$row\n";
   my @values = split('-',$row);
   my $combined_string;
   my $orig_string;

   for(my $i = 0; $i < (@values-1); $i++) {
      $combined_string .= $values[$i];
      $orig_string .= $values[$i]." ";
      #print "Found element $i: $values[$i]\n";
   }
   
   #print "Combined string: $combined_string\n";
   my $ordered_string = rank_order_string($combined_string);
   #my $alphabetized_string = join('', sort { $a cmp $b } split(//, $combined_string));
   #print "Ordered string: $ordered_string\n";

   $row =~ /\[(.*)\]/;
   my $checksum = $1; 
   #print "Checksum: $checksum\n";

   my $res = validate_checksum(alpha_string => $ordered_string, checksum => $checksum);

   if($res){
      #print "Room is valid\n";
      $values[-1] =~ /(.*)\[/;
      #print "Sector ID is $1\n";
      $sector_id_sum += $1;
      my $real_name = break_cypher(orig => $orig_string, id => $1);
      print "Cypher broken for sector id $1: $real_name\n";
   }
   else{
      #print "Room is not valid\n";
   }

   #exit;
}

print "Sum of sector IDs: $sector_id_sum\n";

sub rank_order_string {
   my $string = shift;

   my %letter_counts;

   foreach my $letter (split('',$string)) {
      $letter_counts{$letter}++;
   }

   my $new_string;

   while(%letter_counts){
      my $current_count = 0;
      my $current_letter;
      foreach my $letter (split('',"abcdefghijklmnopqrstuvwxyz")){
         if(defined($letter_counts{$letter}) && $letter_counts{$letter} > $current_count){
            $current_letter = $letter;
            $current_count = $letter_counts{$letter};
         }
      }

      for(my $i = 0; $i < $current_count; $i++){
         $new_string .= $current_letter;
      }

      delete $letter_counts{$current_letter};
   }

   return $new_string;
}
   
sub validate_checksum {
   my %args = @_;

   my $previous_char;
   my $checksum_index = 0;
   my @checksum = split('',$args{checksum});

   # = substr($args{alpha_string},0,0);
   foreach my $character (split('',$args{alpha_string})){
      if(defined($previous_char) && $character eq $previous_char){
         #print "Skipping $character because we've seen it already\n";
         next;
      }

      $previous_char = $character;

      if($character ne $checksum[$checksum_index]){
         #print "Character mismatch comparing $character to ".$checksum[$checksum_index]."\n";
         return 0;
      }
      else{
         #print "Character $character matched to ".$checksum[$checksum_index]."\n";
      }

      $checksum_index++;

      if($checksum_index == 5){
         return 1;
      }
   }
}

sub break_cypher {
   my %args = @_;

   my $alphabet = 'abcdefghijklmnopqrstuvwxyz';
   my @alphabet = split('',$alphabet);

   my @string_chars = split('',$args{orig});

   my $new_string;

   foreach my $char (@string_chars) {
      if ($char eq ' '){
         $new_string .= " ";
         next;
      }
      my $i;
      for($i = 0; $i <= @alphabet; $i++){
         last if $alphabet[$i] eq $char;
      }
      
      $i += $args{id};
      $i = $i % 26;

      $new_string .= $alphabet[$i];
   }

   return $new_string;
}



