#!/usr/bin/python3
# cube game 
import sys
import math
import numpy


input_file=open(sys.argv[1], "r")

power_sum = 0

for input_value in input_file:
    input_value = input_value.strip()
    header_values = input_value.split(":")
    game_id = header_values[0].split(" ")[1]
    #print(game_id)
    
    min_values = {
        "red" : 0,
        "green" : 0,
        "blue" : 0
    }

    for cube_set in header_values[1].split(";"):
        #print("pull: "+cube_set)
        for cube in cube_set.split(","):
            cube_values = cube.strip().split(" ")
            #print(cube)
            if int(cube_values[0]) > int(min_values[cube_values[1]]):
                min_values[cube_values[1]] = int(cube_values[0])
        
    power_sum += numpy.prod(list(min_values.values()))

print(power_sum)
