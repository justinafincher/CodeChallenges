#!/usr/bin/python
# Read file with set of code on each line
# opcode position1 position2 outputposition
# opcode 1 addition
# opcode 2 multiplication
# opcode 99 exit
import sys

input_file=open(sys.argv[1], "r")

for line in input_file:
    line = line.rstrip('\n') 
    line_elements = line.split(',')

    opcode_index = 0
   
    # replace values as prescribed in the problem
    line_elements[1] = 12
    line_elements[2] = 2

    print str(line_elements)
    while 1:
        opcode = int(line_elements[opcode_index])
        if opcode == 99:
            break
        position_1 = int(line_elements[opcode_index+1])
        position_2 = int(line_elements[opcode_index+2])
        output_position = int(line_elements[opcode_index+3])

        if opcode == 1:
            line_elements[output_position] = int(line_elements[position_1]) + int(line_elements[position_2])
        if opcode == 2:
            line_elements[output_position] = int(line_elements[position_1]) * int(line_elements[position_2])

        opcode_index += 4

    output_string = ""
    for element in line_elements:
        output_string += str(element)+","
    
    print output_string
