use strict;
use warnings;
use Data::Dumper;

my $filename = 'day7input';
#$filename = 'day7testinput2';

open(my $fh, '<:encoding(UTF-8)', $filename)
  or die "Could not open file '$filename' $!";

my @strings;
my $supported_ips = 0;

MAIN_LOOP:
while (my $row = <$fh>) {
   chomp $row;
   print "$row\n";
   # store strings as within and outside brackets
   my @values = split('',$row);
   my @inside_brackets;
   my @outside_brackets;

   my $currently_inside = 0;
   my $temp_string;
   foreach my $letter (@values) {
      if($letter eq '['){
         $currently_inside = 1;
         if(defined($temp_string)){
            push(@outside_brackets,$temp_string);
            $temp_string = undef;
         }
         next;
      }
      elsif($letter eq ']'){
         $currently_inside = 0;
         if(defined($temp_string)){
            push(@inside_brackets,$temp_string);
            $temp_string = undef;
         }
         next;
      }
      
      $temp_string .= $letter;
   }
   if($currently_inside && defined($temp_string)){
      push(@inside_brackets,$temp_string);
   }
   elsif(defined($temp_string)){
      push(@outside_brackets,$temp_string);
   }


   #print "Strings inside brackets:".Dumper(@inside_brackets);
   #print "Strings outside brackets:".Dumper(@outside_brackets);
   
   foreach my $string (@outside_brackets) {
      my $res = find_abas($string);
      if($res){
         foreach my $other_string (@inside_brackets){
            foreach my $aba (@{$res}) {
               print "Looking for $aba in $other_string\n";
               if(index($other_string,$aba) >= 0){
                  print "Found ABA $aba in $other_string so IP is SSL\n";
                  $supported_ips++;
                  next MAIN_LOOP;
               }
            }
         }
         #next MAIN_LOOP;
      }
   }
   
   print "No ABAs found so IP is invalid\n";
}

print "Found $supported_ips valid IP addresses\n";

sub find_abas {
   my $string = shift;

   my @reverse_abas;

   my @values = split('',$string);

   for(my $i = 2; $i < @values; $i++){
      #my $temp1 = $values[$i-2].$values[$i-1].$values[$i];
      my $temp2 = $values[$i-1].$values[$i].$values[$i-1];
   
      if($values[$i-2] eq $values[$i] && $values[$i-2] ne $values[$i-1]){
         print "Adding the reverse aba, $temp2, to the list\n";
         push(@reverse_abas,$temp2);
      }
   }

   if(@reverse_abas){
      return \@reverse_abas;
   }
   else{
      return undef;
   }
}
