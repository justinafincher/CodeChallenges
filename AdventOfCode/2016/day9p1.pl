use strict;
use warnings;
use Data::Dumper;

my $filename = 'day9input';
#$filename = 'day9testinput';

open(my $fh, '<:encoding(UTF-8)', $filename)
  or die "Could not open file '$filename' $!";

MAIN_LOOP:
while (my $row = <$fh>) {
   chomp $row;
   
   my $decompressed_string;

   print "$row\n";
   
   # store strings as within and outside brackets
   my @values = split('',$row);

   for(my $i = 0;  $i < @values; $i++) {
      print "Evaluating letter at index $i\n";
      print "Evaluating letter |".$values[$i]."|\n";
      if($values[$i] eq '('){
         my $repeat_instructions;
         my $counter = $i+1;
         while($values[$counter] ne ")"){
            $repeat_instructions .= $values[$counter];
            $counter++;
         }
         my @repeat_instructions = split('x',$repeat_instructions);
         my $num_letters = $repeat_instructions[0];
         my $repeats = $repeat_instructions[1];
  
         my $base_index = $counter+1;

         my $repeat_string;
         for(my $j = 0; $j < $num_letters; $j++){
            $repeat_string .= $values[$base_index+$j];
         }

         print "Repeating the next $num_letters for $repeats times\n";
         print "Found repeat string of $repeat_string\n";
         
         for(my $j = 0; $j < $repeats; $j++){
            $decompressed_string .= $repeat_string;
         }
         $i = $base_index+$num_letters-1;
         print "Added repeats to string: $decompressed_string resetting index to $i which points at ".$values[$i]."\n";
         #next;
      }
      else {
         $decompressed_string .= $values[$i];
      }
   }
   print "Decompressed string: $decompressed_string which has a length of ".length($decompressed_string)."\n";
   #last;
   #print "Decompressed string is of length ".length($temp_string)."\n";
}

