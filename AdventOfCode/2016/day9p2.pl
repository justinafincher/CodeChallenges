use strict;
use warnings;
use Data::Dumper;

my $filename = 'day9input';
#$filename = 'day9testinput2a';
#$filename = 'day9testinput2c';

my $working1 = "day9working1";
my $working2 = "day9working2";
my $building = "day9workingbuilding";
my $remaining = "day9workingremaining";

system("cp $filename $working1");

open(my $fh, '<:encoding(UTF-8)', $working1)
  or die "Could not open file '$working1' $!";

open(my $out_fh, '>:encoding(UTF-8)', $working2)
   or die "Could no open outfile $!";

my $character;
read $fh, $character, 1;
MAIN_LOOP:
while ($character ne "") {
   #print "Read |$character|\n";
   
   if($character eq "("){
      $fh = decompress_string($fh, $out_fh); 
   }
   else{
      print $out_fh $character;
   }
   
   read $fh, $character, 1;

   # if we have finished, check if we have finished
   if($character eq "" && system("diff -q $working1 $working2")){
      #print "Finished processing, but more processing to do\n";
      #files are different. keep going
      close($fh);
      close($out_fh);

      system("mv $working2 $working1");

      # store the preaamble with no repeats TODO
      open($fh, '<:encoding(UTF-8)', $working1)
        or die "Could not open file '$working1' $!";

      open(my $out_fh_preamble, '>>:encoding(UTF-8)', $building)
        or die "Could not open file '$building' $!";

      open(my $out_fh_remaining, '>:encoding(UTF-8)', $remaining)
        or die "Could not open file '$remaining' $!";

      read $fh, $character, 1;
      while ($character ne "" && $character ne "(") {
         print $out_fh_preamble $character;
         read $fh, $character, 1;
      }
      close($out_fh_preamble);

      while($character ne ""){
         print $out_fh_remaining $character;
         read $fh, $character, 1;
      }
      close($out_fh_remaining);
      close($fh);

      system("cp $remaining $working1");
      
      open($fh, '<:encoding(UTF-8)', $working1)
        or die "Could not open file '$working1' $!";

      open($out_fh, '>:encoding(UTF-8)', $working2)
         or die "Could no open outfile $!";
      
      read $fh, $character, 1;
   }
   elsif($character eq ""){
      #print "Finished after reading characeter |$character|\n";
      last;
   }
}

# Read file and count the characters
open($fh, '<:encoding(UTF-8)', $building)
  or die "Could not open file '$building' $!";

read $fh, $character, 1;

my $character_count = 0;
while ($character ne "" && $character ne "\n") {
   $character_count++;
   read $fh, $character, 1;
}

print "Decompressed string is $character_count characters long\n";

   
sub decompress_string {
   my $in_file_handle = shift;
   my $out_file_handle = shift;

   my $character = "";
   my $repeat_instructions = "";
   
   read $in_file_handle, $character, 1;
   while($character ne ")"){
      $repeat_instructions .= $character;
      read $in_file_handle, $character, 1;
   }
   #print "Found repeat instructions $repeat_instructions\n";
   my @repeat_instructions = split('x',$repeat_instructions);
   my $num_letters = $repeat_instructions[0];
   my $repeats = $repeat_instructions[1];

   my $repeat_string = "";
   my $new_char;
   for(my $i = 0; $i < $num_letters; $i++){
      read $in_file_handle, $new_char, 1;
      $repeat_string .= $new_char;
   }
    
   for(my $i = 0; $i < $repeats; $i++){
      print $out_file_handle $repeat_string;
   }
  
   return $in_file_handle; 
   #my $continue = 0;
   ## store strings as within and outside brackets
   #my @values = split('',$row);

   #for(my $i = 0;  $i < @values; $i++) {
   #   #print "Evaluating letter at index $i\n";
   #   #print "Evaluating letter |".$values[$i]."|\n";
   #   if($values[$i] eq '('){
   #      $continue = 1;
   #      my $repeat_instructions;
   #      my $counter = $i+1;
   #      while($values[$counter] ne ")"){
   #         $repeat_instructions .= $values[$counter];
   #         $counter++;
   #      }
   #      my @repeat_instructions = split('x',$repeat_instructions);
   #      my $num_letters = $repeat_instructions[0];
   #      my $repeats = $repeat_instructions[1];
  
   #      my $base_index = $counter+1;

   #      my $repeat_string;
   #      for(my $j = 0; $j < $num_letters; $j++){
   #         $repeat_string .= $values[$base_index+$j];
   #      }

   #      #print "Repeating the next $num_letters for $repeats times\n";
   #      #print "Found repeat string of $repeat_string\n";
   #      
   #      for(my $j = 0; $j < $repeats; $j++){
   #         $decompressed_string .= $repeat_string;
   #      }
   #      $i = $base_index+$num_letters-1;
   #      #print "Added repeats to string: $decompressed_string resetting index to $i which points at ".$values[$i]."\n";
   #      #next;
   #   }
   #   else {
   #      $decompressed_string .= $values[$i];
   #   }
   #}
   #
   #return ($decompressed_string,$continue);
   #last;
   #print "Decompressed string is of length ".length($temp_string)."\n";
}
