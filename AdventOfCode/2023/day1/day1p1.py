#!/usr/bin/python3
# read calibration values, combine first and last digits, then sum
import sys
import math

input_file=open(sys.argv[1], "r")

calibrations = []

for input_value in input_file:
    input_value = input_value.strip()
    temp_string = ""
    for character in input_value:
        if character.isnumeric():
            temp_string += character
    calibrations.append(int(temp_string[0]+temp_string[-1]))

print(sum(calibrations))
