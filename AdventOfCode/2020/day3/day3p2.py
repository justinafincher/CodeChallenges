#!/usr/bin/python
# Given map of trees, you have a sled that traverses by going down 1 and over 3. Given this
# trajectory, how many trees will you hit by the time you hit the bottom?
import sys
import math
import re
import numpy

x_movements = [1, 3, 5, 7, 1]

all_trees_hit = []

for i in range(5):
    input_file=open(sys.argv[1], "r")

    trees_hit = 0
    x_val = 0
    process_row = 1 # bool to skip on last check

    y_val = 0
    # Each line in form . for open and # for tree
    for input_value in input_file:
        if i == 4 and process_row == 0:
            process_row = 1
            continue
        
        input_value = input_value.strip()
        index_to_check = x_val % len(input_value)
   
        if input_value[index_to_check] == "#":
            trees_hit += 1
    
        x_val = index_to_check + x_movements[i] 
    
        process_row = 0
    
    print "For iteration " + str(i) + " hit " + str(trees_hit)

    all_trees_hit.append(trees_hit)

print(numpy.prod(all_trees_hit))
