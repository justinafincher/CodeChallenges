#!/usr/bin/python
# Ferry navigation  
import sys
import math

def change_current_direction(direction, current_direction):
    if direction == "L":
        if current_direction == "N":
            current_direction = "W"
        elif current_direction == "S":
            current_direction = "E"
        elif current_direction == "E":
            current_direction = "N"
        elif current_direction == "W":
            current_direction = "S"
    elif direction == "R":
        if current_direction == "N":
            current_direction = "E"
        elif current_direction == "S":
            current_direction = "W"
        elif current_direction == "E":
            current_direction = "S"
        elif current_direction == "W":
            current_direction = "N"

    return current_direction

# x, y
current_direction = "E"
current_position = [0, 0]

# Action N means to move north by the given value.
# Action S means to move south by the given value.
# Action E means to move east by the given value.
# Action W means to move west by the given value.
# Action L means to turn left the given number of degrees.
# Action R means to turn right the given number of degrees.
# Action F means to move forward by the given value in the direction the ship is currently facing.
input_file=open(sys.argv[1], "r")
for input_value in input_file:
    input_value = input_value.strip()

    direction_action = input_value[0]
    direction_distance = int(input_value[1:])
    print "Direction " + direction_action + " distance " + str(direction_distance)

    if direction_action == "F":
        direction_action = current_direction
    
    if direction_action == "N":
        current_position[1] += direction_distance
    elif direction_action == "S":
        current_position[1] -= direction_distance
    elif direction_action == "E":
        current_position[0] += direction_distance
    elif direction_action == "W":
        current_position[0] -= direction_distance
    elif direction_action == "L" or direction_action == "R":
        for i in range(direction_distance / 90):
            current_direction = change_current_direction(direction_action, current_direction)

print str(abs(current_position[0]) + abs(current_position[1]))
