/* Hidden stub code will pass a root argument to the function below. Complete the function to solve the challenge. Hint: you may want to write one or more helper functions.  

The Node class is defined as follows:
    class Node {
        int data;
        Node left;
        Node right;
     }
*/
    
    boolean checkBST(Node root) {
        if((root.left != null && root.data < root.left.data) || (root.right != null && root.data > root.right.data)){
            //System.out.println("Failed first check");
            return false;
        }
        
        //System.out.println("Evaluating left node");
        if(root.left != null){
            int left_result = findMaxInTree(root.left, 0);
            //System.out.println("Found max of "+left_result+" in left tree. Current node is "+root.data);
            if(left_result >= root.data){
                return false;
            }
            
            boolean left_check = checkBST(root.left);
            
            if(left_check == false){
                return false;
            }
        }
        
        //System.out.println("Evaluating right node");
        if(root.right != null){
            int right_result = findMinInTree(root.right, 2147483647);
            //System.out.println("Found min of "+right_result+" in right tree. Current node is "+root.data);

            if(right_result <= root.data){
                return false;
            }
            
            boolean right_check = checkBST(root.right);
            
            if(right_check == false){
                return false;
            }
        }
        
        return true;
    }

    int findMaxInTree(Node parent, int currentValue){
        if(parent.left != null){
            int temp1 = findMaxInTree(parent.left, currentValue);
            currentValue = Math.max(currentValue,temp1);
            //System.out.println("Evaluated left node to find max of "+Integer.toString(temp1));
        }
        else{
            currentValue = Math.max(parent.data,currentValue);
        }
        
        if(parent.right != null){
            int temp2 = findMaxInTree(parent.right, currentValue);
            currentValue = Math.max(currentValue,temp2);
            //System.out.println("Evaluated right node to find max of "+Integer.toString(temp2));
        }
        else{
            currentValue = Math.max(parent.data,currentValue);
        }
        
        return currentValue;
    }

    int findMinInTree(Node parent, int currentValue){
        if(parent.left != null){
            int temp1 = findMinInTree(parent.left, currentValue);
            currentValue = Math.min(currentValue,temp1);
            //System.out.println("Evaluated left node to find min of "+Integer.toString(temp1));
        }
        else{
            currentValue = Math.min(parent.data,currentValue);
        }
        
        if(parent.right != null){
            int temp2 = findMinInTree(parent.right, currentValue);
            currentValue = Math.min(currentValue,temp2);
            //System.out.println("Evaluated left node to find min of "+Integer.toString(temp2));
        }
        else{
            currentValue = Math.min(parent.data,currentValue);
        }
        
        return currentValue;
    }
