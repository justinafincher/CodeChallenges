#!/usr/bin/python3
# Given crate stacks, calculate final orientation. 
import sys
import math
import re

def process_move(stacks, move_from, move_count, move_to):
    for i in range(0, move_count):
        temp_value = stacks[move_from].pop()
        stacks[move_to].append(temp_value)

    return stacks


input_file=open(sys.argv[1], "r")
is_building_stack = True
stacks = {}
temp_stacks = []

for input_value in input_file:
    input_value = input_value.rstrip('\n')
    num_stacks = int((len(input_value)-3)/4 + 1)

    # first build crate stack
    if is_building_stack:
        # we have all the stack values
        if input_value.strip() == "":
            # process the stack
            print("done reading in stacks")
            is_building_stack = False
            stack_labels = temp_stacks.pop()
            for i in range(1, len(stack_labels), 4):
                stacks[stack_labels[i]] = []

            for i in range(0, len(temp_stacks)):
                temp_line = temp_stacks.pop()
                for j in range(1, len(temp_line), 4):
                    if temp_line[j] != " ":
                        stacks[stack_labels[j]].append(temp_line[j])
        else:
            temp_stacks.append(input_value)
    # process directions
    else:
        m = re.match("move (\d+) from (\d+) to (\d+)", input_value.strip())
        stacks = process_move(stacks, m.group(2), int(m.group(1)), m.group(3))

print(stacks)

# print tops of stacks
tops = ""
for key in stacks.keys():
    tops += stacks[key][-1]
print(tops)
