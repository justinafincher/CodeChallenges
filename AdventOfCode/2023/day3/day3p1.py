#!/usr/bin/python3
# process engine schematic for gondola 
import sys
import math
from collections import namedtuple

non_symbols = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "."]

Point = namedtuple("Point", ["x", "y"])

# Take an array of points and an array of symbol locations and determine if there's any adjacency
def is_valid(test_points, symbol_array):
    for point in test_points:
        for symbol in symbol_array:
            if abs(point[0]-symbol[0]) < 2 and abs(point[1]-symbol[1]) < 2:
                return True
    return False

input_file=open(sys.argv[1], "r")


number_sum = 0

schematic = []

for input_value in input_file:
    input_value = input_value.strip()
    temp_values = []
    for character in input_value:
        temp_values.append(character)
    schematic.append(temp_values)

symbols = []

# Note locations of symbols
for i in range(0,len(schematic)):
    for j in range(0,len(schematic[i])):
        if schematic[i][j] not in non_symbols:
            symbols.append(Point(i, j))
            #print("Adding non symbol: "+schematic[i][j])

#print(symbol_x)
#print(symbol_y)

# Find numbers then check if adjacent to symbol
building_number = False

temp_num = ""
temp_coords = []

for i in range(0,len(schematic)):
    for j in range(0,len(schematic[i])):
        #print("Evaluating char: "+schematic[i][j])
        if schematic[i][j].isnumeric() and building_number:
            #print("continuing started number")
            temp_num += schematic[i][j]
            temp_coords.append(Point(i,j))
        elif schematic[i][j].isnumeric():
            #print("starting new number")
            temp_num = schematic[i][j]
            building_number = True
            temp_coords.append(Point(i,j))
        elif not schematic[i][j].isnumeric() and building_number:
            print("Found number "+temp_num)
            # test if valid
            if is_valid(temp_coords, symbols):
                print("number adjacent to symbol and is valid")
                number_sum += int(temp_num)
            else:
                print("invalid")
            temp_coords = []
            temp_num = ""
            building_number = False
        else:
            continue

    if building_number:
        end_coord = Point(i,j-1)
        # also test valid here
        print("found number at end of line")
        if is_valid(temp_coords, symbols):
            print("number adjacent to symbol and is valid")
            number_sum += int(temp_num)
    building_number = False
    temp_num = ""

print(number_sum)
