#!/usr/bin/python
# process docking data  
import sys
import math
import re

def mask_binary(binary_string, mask):
    # pad out binary if not already long enough
    length_difference = len(mask) - len(binary_string)
    if length_difference < 0:
        print "ERROR: mask or binary string length incorrect"
        return "ERROR"
    for i in range(0, length_difference):
        binary_string = "0" + binary_string

    print binary_string

    for i in range(0, len(mask)):
        if mask[i] != "X":
            temp_string = binary_string[:i]
            temp_string += mask[i]
            temp_string += binary_string[i+1:]
            binary_string = temp_string

    return binary_string

def convert_to_binary(value):
    binary_string = ""
    if value > 1:
        binary_string += str(convert_to_binary(value // 2))

    binary_string += str(value % 2)

    return binary_string

mem = {}

input_file=open(sys.argv[1], "r")

for input_value in input_file:
    input_value = input_value.strip()
    print input_value
    if input_value[0:4] == "mask":
        mask = input_value[7:]
        print "Found mask " + mask
    else:
        memory_address = int(re.split('[\[\]]', input_value)[1])
        memory_value = int(input_value.split(" ")[2])
        print "Processing memory at address " + str(memory_address)
        print "Processing value " + str(memory_value)
        temp_binary = convert_to_binary(memory_value)
        mem[int(memory_address)] = mask_binary(temp_binary, mask)

print mem
mem_sum = 0
for location in mem.keys():
    mem_sum += int(mem[location], 2)

print mem_sum
