#!/usr/bin/python
# generate all paths through caves 
import sys
import math
import numpy

connections = {}
paths = ["start"]

def find_next_step_in_path(paths):
    new_paths = []
    for path in paths:
        print "Check for end in " + path
        print path.split("-")[-1]
        if path.split("-")[-1] == "end":
            print "Found end, check next path"
            new_paths.append(path)
        else:
            print "Didn't end with end, so let's continue"
            for option in connections[path.split("-")[-1]]:
                if (option.islower() and "-"+option+"-" not in path) or option.isupper():
                    new_paths.append(path + "-" + option)

    return new_paths


input_file=open(sys.argv[1], "r")

for input_value in input_file:
    input_values = input_value.strip().split("-")
    print input_value
    if input_values[0] not in connections.keys():
        connections[input_values[0]] = []
    if input_values[1] not in connections.keys():
        connections[input_values[1]] = []
    
    if input_values[0] == "start":
        connections[input_values[0]].append(input_values[1])
    elif input_values[1] == "start":
        connections[input_values[1]].append(input_values[0])
    else:
        connections[input_values[0]].append(input_values[1])
        connections[input_values[1]].append(input_values[0])

print connections

previous_paths_length = 0
while(len(paths) != previous_paths_length):
    previous_paths_length = len(paths)
    paths = find_next_step_in_path(paths)
    print paths
    

print len(paths)

