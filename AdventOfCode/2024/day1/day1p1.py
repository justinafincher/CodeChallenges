#!/usr/bin/python3
# With list of pairs of numbers, starting with smallest numbers and add up differences 
import sys
import math

input_file=open(sys.argv[1], "r")

left_list = []
right_list = []
distance_sum = 0


for input_value in input_file:
    input_value = input_value.strip()
    input_values = input_value.split()

    left_list.append(input_values[0])
    right_list.append(input_values[1])

left_list = sorted(left_list)
right_list = sorted(right_list)

if len(left_list) != len(right_list):
    print("Lists not the same length")
    exit

for i in range(0,len(left_list)):
    distance_sum += abs(int(left_list[i]) - int(right_list[i]))

print(f"Distance: {distance_sum}")
