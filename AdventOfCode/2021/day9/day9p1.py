#!/usr/bin/python
# Given elevation map, evaluation low points 
import sys
import math
import numpy

elevation_map = []

input_file=open(sys.argv[1], "r")
border = ""
first_line = 1
for input_value in input_file:
    input_value_line = input_value.strip()
    print input_value_line
    if first_line == 1:
        for i in range(0, len(input_value_line) + 2):
            border += "9"
        elevation_map.append(border)
        first_line = 0

    elevation_map.append(("9" + input_value_line + "9"))

elevation_map.append(border)

low_points = []
for row in range(1, len(elevation_map)-1):
    for column in range(1, len(elevation_map[row])-1):
        is_low_point = 1
        
        if elevation_map[row-1][column] <= elevation_map[row][column]:
            is_low_point = 0
        if elevation_map[row+1][column] <= elevation_map[row][column]:
            is_low_point = 0
        if elevation_map[row][column-1] <= elevation_map[row][column]:
            is_low_point = 0
        if elevation_map[row][column+1] <= elevation_map[row][column]:
            is_low_point = 0
        
        if is_low_point == 1:
            low_points.append(str(row)+","+str(column))

score = 0
for point in low_points:
    score += int(elevation_map[int(point.split(",")[0])][int(point.split(",")[1])]) + 1

print score
