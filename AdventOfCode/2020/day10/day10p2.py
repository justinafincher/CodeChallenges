#!/usr/bin/python
# Evaluate "joltage" adapter combinations 
import sys
import math
import numpy

input_file=open(sys.argv[1], "r")

values = [0]

for input_value in input_file:
    input_value = input_value.strip()
    values.append(int(input_value))

values.sort()

ones_counter = 0
runs_of_one = []
for i in range(0, len(values) - 1):
    if values[i+1] - values[i] == 1:
        ones_counter += 1
    else:
        if ones_counter > 1:
            runs_of_one.append(ones_counter)
        ones_counter = 0
if ones_counter > 1:
    runs_of_one.append(ones_counter)

combinations = []
for run in runs_of_one:
    combination = int((0.5 * run * run) - (0.5 * run) + 1)
    combinations.append(combination)

print numpy.prod(combinations)
