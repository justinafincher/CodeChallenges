use strict;
use warnings;
use Data::Dumper;

my $input = "L2, L5, L5, R5, L2, L4, R1, R1, L4, R2, R1, L1, L4, R1, L4, L4, R5, R3, R1, L1, R1, L5, L1, R5, L4, R2, L5, L3, L3, R3, ".
   "L3, R4, R4, L2, L5, R1, R2, L2, L1, R3, R4, L193, R3, L5, R45, L1, R4, R79, L5, L5, R5, R1, L4, R3, R3, L4, R185, L5, L3, L1, R5, L2, ".
   "R1, R3, R2, L3, L4, L2, R2, L3, L2, L2, L3, L5, R3, R4, L5, R1, R2, L2, R4, R3, L4, L3, L1, R3, R2, R1, R1, L3, R4, L5, R2, R1, R3, ".
   "L3, L2, L2, R2, R1, R2, R3, L3, L3, R4, L4, R4, R4, R4, L3, L1, L2, R5, R2, R2, R2, L4, L3, L4, R4, L5, L4, R2, L4, L4, R4, R1, R5, ".
   "L2, L4, L5, L3, L2, L4, L4, R3, L3, L4, R1, L2, R3, L2, R1, R2, R5, L4, L2, L1, L3, R2, R3, L2, L1, L5, L2, L1, R4";

#$input = "R5, L5, R5, R3";
#$input = "R2, R2, R2";
#$input = "R23, L3";

my @inputs = split(',',$input);

my $xcoord = 0;
my $ycoord = 0;
my $direction = 0; # 0 = N, 1 = E, 2 = S, 3 = W

foreach my $value (@inputs) {
   $value =~ s/^\s+|\s+$//g;
   my @directions = split('',$value);
   $value = substr($value, 1, 1000);

   if($directions[0] eq 'L'){
      $direction--;
      #print "$direction is direction after turning left\n";
      if($direction != 0){
         $direction = $direction % 4;
      }
   }
   else{
      $direction++;
      #print "$direction is direction after turning right\n";
      if($direction != 0){
         $direction = $direction % 4;
      }
   }

   if($direction == 0){
      $ycoord += $value;
   }
   elsif($direction == 1){
      $xcoord += $value;
   }
   elsif($direction == 2){
      $ycoord -= $value;
   }
   else{
      $xcoord -= $value;
   }

   print "At $xcoord,$ycoord after processing direction $value\n";
#"|$value|\n";
   #last;
}

print "Current position, $xcoord,$ycoord and distance is ".(abs($xcoord)+abs($ycoord))."\n";
