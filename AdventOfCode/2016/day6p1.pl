use strict;
use warnings;
use Data::Dumper;

my $filename = 'day6input';
#$filename = 'day6testinput';

open(my $fh, '<:encoding(UTF-8)', $filename)
  or die "Could not open file '$filename' $!";

my @strings;
my $counter = 0;

while (my $row = <$fh>) {
   chomp $row;
   print "$row\n";
   push(@strings,$row);
   $counter++;
}

my $length = length($strings[0]);

my $message;

for(my $i = 0; $i < $length; $i++){
   my %letter_counts;
   for(my $j = 0; $j < $counter; $j++){
      $letter_counts{substr($strings[$j],$i,1)}++;
   }
   my $max = 0;
   my $max_letter;
   foreach my $letter (keys %letter_counts){
      if($letter_counts{$letter} > $max){
         $max = $letter_counts{$letter};
         $max_letter = $letter;
      }
   }
   
   $message .= $max_letter;
   #print Dumper(%letter_counts);
   #last;
}

print "Decrypted message: |$message|\n";
