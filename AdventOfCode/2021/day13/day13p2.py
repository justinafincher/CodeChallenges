#!/usr/bin/python
# fold transparent paper 
import sys
import math
import numpy

numpy.set_printoptions(threshold=numpy.inf)

def distinct_list(list_of_points):
    new_list = []
    for point in list_of_points:
        if point not in new_list:
            new_list.append(point)

    return new_list


input_file=open(sys.argv[1], "r")

folding_instructions = []
holes = []
for input_value in input_file:
    input_value = input_value.strip()
    input_values = input_value.split(",")
    if len(input_values) > 1:
        holes.append(list(map(int, input_values)))
    elif len(input_value) > 0:
        input_value = input_value[11:]
        folding_instructions.append(input_value)

# to calculate new hole locations, loop through the instructions and mod the values appropriately
for instruction in folding_instructions:
    instruction_parts = instruction.split("=")
    direction = instruction_parts[0]
    fold_line = int(instruction_parts[1])
    if direction == "y":
        for i in range(0, len(holes)):
            if holes[i][1] > fold_line:
                holes[i][1] = holes[i][1] - (2*(holes[i][1] - fold_line))
    elif direction == "x":
        for i in range(0, len(holes)):
            if holes[i][0] > fold_line:
                holes[i][0] = holes[i][0] - (2*(holes[i][0] - fold_line))
    else:
        print "Errant fold value"
        print instruction

holes = distinct_list(holes)
print len(holes)

# print out the sheet
paper_sheet = numpy.full((50, 50),".")
for hole in holes:
    paper_sheet[int(hole[1]),int(hole[0])] = "#"

for i in range(0, 40, 5):
    letter_temp = paper_sheet[range(0,6),:]
    letter = letter_temp[:,range(i,i+4)]
    print letter
