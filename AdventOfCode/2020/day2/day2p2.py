#!/usr/bin/python
# Password validation: Each line gives the password policy and then the password. The password 
# policy indicates the lowest and highest number of times a given letter must appear for the 
# password to be valid. For example, 1-3 a means that the password must contain a at least 1 time 
# and at most 3 times.
#
# Part 2, numbers aren't min and max, but positions. Letter must exist in one and only one of the
# positions.

import sys
import math
import re

input_file=open(sys.argv[1], "r")

valid_passwords = 0

# Each line in form 1-4 a: abcde
# 1: first position to check
# 4: second position to check
# a: character to check
# abcde: example password
# Count the number of passwords that meet the associated requirement
for input_value in input_file:
    split_string = re.split('[- :]', input_value.strip())
    first_position = int(split_string[0])-1
    second_position = int(split_string[1])-1
    character = split_string[2]
    password = split_string[4]
    
    if (password[first_position] == character and password[second_position] != character) or (password[first_position] != character and password[second_position] == character):
        valid_passwords += 1
    
print valid_passwords
