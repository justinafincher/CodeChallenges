"""
Detect a cycle in a linked list. Note that the head pointer may be 'None' if the list is empty.

A Node is defined as: 
 
    class Node(object):
        def __init__(self, data = None, next_node = None):
            self.data = data
            self.next = next_node
"""

def has_cycle(head):
   visited_nodes = [head.data]

   current_node = head.next

   while current_node is not None:
      if current_node.data in visited_nodes:
         reutrn True
      visited_nodes.append(current_node.data)
      current_node = current_node.next
    
   return False
    

