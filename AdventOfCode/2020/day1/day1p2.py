#!/usr/bin/python
# Read file with int on each line. Find trio of numbers whose sum is 2020. Return product.
import sys
import math

input_file=open(sys.argv[1], "r")

account_values = []

for input_value in input_file:
    account_values.append(int(input_value))

for i in range(0, len(account_values)-2):
    for j in range(1, len(account_values)-1):
        for k in range(2, len(account_values)):
            if account_values[i] + account_values[j] + account_values[k] == 2020:
                print "Found 2020 sum. Product is " + str(account_values[i] * account_values[j] * account_values[k])
