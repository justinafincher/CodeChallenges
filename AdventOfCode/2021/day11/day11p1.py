#!/usr/bin/python
# simulat dumbo octopus energies 
import sys
import math
import numpy

octopus_energies = numpy.full((10, 10),0)

# going to be terrible and just use the global octopus_energies here
def update_energies():
    flashes = []
    for i in range(0, 10):
        for j in range(0, 10):
            if int(octopus_energies[i,j]) >= 9:
                if [i,j] not in flashes:
                    flashes.append([i,j]) 
                #octopus_energies[i,j] = 0
            
            octopus_energies[i,j] = int(octopus_energies[i,j]) + 1
    return flashes

def determine_flashes(processed_flashes, new_flashes):
    added_flashes = 0
    print "processing set of flashes"
    print new_flashes
    newly_found_flashes = []
    #print len(flashes)
    for flash in new_flashes:
        #print "processing flash"
        #print flash
        for i in range(max(flash[0]-1, 0), min(flash[0]+2,10)):
            for j in range(max(flash[1]-1,0), min(flash[1]+2,10)):
                #if flash == [4,9]:
                #    print "Processing flash of interest"
                #    print [i,j]
                if (i == flash[0] and j == flash[1]) or [i,j] in processed_flashes+new_flashes:
                    next
                else:
                    octopus_energies[i,j] += 1
                
                if int(octopus_energies[i,j]) > 9 and [i,j] not in processed_flashes+new_flashes+newly_found_flashes:
                    #print "Adding new flash"
                    #print [i,j]
                    #print processed_flashes+new_flashes
                    newly_found_flashes.append([i,j])
                    added_flashes += 1
                    
        octopus_energies[flash[0],flash[1]] = 0
        processed_flashes.append(flash)

    print "done processing flashes"
    print octopus_energies
    print processed_flashes
    print len(processed_flashes)

    if added_flashes == 0:
        print "No added flashes"
        return len(processed_flashes)
    else:
        print "Added " + str(len(newly_found_flashes)) + " additional flashes"
        
        return determine_flashes(processed_flashes, newly_found_flashes)


def reset_flashes():
    for i in range(0, 10):
        for j in range(0, 10):
            if octopus_energies[i,j] > 9:
                octopus_energies[i,j] = 0

input_file=open(sys.argv[1], "r")

row_num = 0
for input_value in input_file:
    input_value_line = input_value.strip()
    print input_value_line

    for i in range(0, len(input_value_line)):
        octopus_energies[row_num][i] = input_value_line[i]

    row_num += 1

iterations = 100
print "Starting configuration"
print octopus_energies

total_flashes = 0
for i in range(0, iterations):
    flashes = update_energies()
    print "Added initial energies"
    print octopus_energies
    #print flashes
    num_flashes = determine_flashes([], flashes)
    print num_flashes
    total_flashes += int(num_flashes)
    reset_flashes()
    print "Completed step " + str(i+1)
    print octopus_energies

print total_flashes
