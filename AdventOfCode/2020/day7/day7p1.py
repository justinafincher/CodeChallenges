#!/usr/bin/python
# Use bag descriptions to calculate possible container bags

# First modified the inputs with the following
# :%s/\([a-z]* [a-z]*\) bags contain [0-9]* /\1:/
# :%s/ bag[s]*, [0-9]* /:/g
# :%s/ bag[s]*.//

import sys
import math
import re

input_file=open(sys.argv[1], "r")

# each key contains a list of which bags can hold said bag
# e.g. organization_map["blue"] = "red" means a blue bag can be held by a red bag.
organization_map = {}
bags_found = {}

def find_bags(bag_of_interest):
    if organization_map.has_key(bag_of_interest):
        for bag in organization_map[bag_of_interest]:
            bags_found[bag] = 1
            find_bags(bag)
        return
    else:
        bags_found[bag_of_interest] = 1
        return

# Hold all bags seen
bags_seen = {}

interest_bag = "shiny gold"
possible_bags = 0

for input_value in input_file:
    input_value = input_value.strip()
    bag_infos = input_value.split(":")
    target_bag = bag_infos[0]

    for bag in bag_infos:
        if bag == target_bag:
            continue
        if bag in organization_map:
            organization_map[bag].append(target_bag)
        else:
            organization_map[bag] = [target_bag]
    
find_bags(interest_bag)
print sum(bags_found.values())
