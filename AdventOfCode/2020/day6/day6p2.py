#!/usr/bin/python
# Sum counts of customs forms answer 

import sys
import math
import re

# instead of total yesses, count where entire group answered yes
def calculate_group_score(group_answers, group_size):
    yes_questions = {}
    for i in range(0, len(group_answers)):
        if group_answers[i] in yes_questions:
            yes_questions[group_answers[i]] += 1
        else:
            yes_questions[group_answers[i]] = 1

    # Count questions that all answered
    all_yesses = 0
    
    for question_count in yes_questions.values():
        if int(question_count) == group_size:
            all_yesses += 1
    
    return all_yesses

input_file=open(sys.argv[1], "r")

cumulative_yesses = 0
group_answers = ""
group_size = 0

for input_value in input_file:
    input_value = input_value.strip()

    # Blank line separates
    if len(input_value) == 0:
        cumulative_yesses += calculate_group_score(group_answers, group_size)
        group_answers = ""
        group_size = 0
        continue

    group_answers += input_value
    group_size += 1

# and the last one
cumulative_yesses += calculate_group_score(group_answers, group_size)
    
print cumulative_yesses
