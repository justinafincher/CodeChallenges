#!/usr/bin/perl

use Data::Dumper;

$a = <STDIN>;
chomp $a;
$b = <STDIN>;
chomp $b;

my %a_dict;
my %b_dict;
my %total_dict;

foreach my $char (split('',$a)){
    $a_dict{$char}++;
    unless(exists($total_dict{$char})){
        $total_dict{$char} = 1;
    }
}

foreach my $char (split('',$b)){
    $b_dict{$char}++;
    unless(exists($total_dict{$char})){
        $total_dict{$char} = 1;
    }
}

my $differences = 0;

foreach my $char (keys %total_dict){
    if(exists($a_dict{$char}) && exists($b_dict{$char})){
        $differences += abs($a_dict{$char}-$b_dict{$char});
    }
    elsif(exists($a_dict{$char})){
        $differences += $a_dict{$char};
    }
    else{
        $differences += $b_dict{$char};
    }
}

print $differences."\n";
