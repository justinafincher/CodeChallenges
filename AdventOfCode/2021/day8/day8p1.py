#!/usr/bin/python
#  
import sys
import math
import numpy

# display map
#  000
# 1   2
# 1   2
#  333
# 4   5
# 4   5
#  666

# Notes
#   0:      1:      2:      3:      4:
# aaaa    ....    aaaa    aaaa    ....
#b    c  .    c  .    c  .    c  b    c
#b    c  .    c  .    c  .    c  b    c
# ....    ....    dddd    dddd    dddd
#e    f  .    f  e    .  .    f  .    f
#e    f  .    f  e    .  .    f  .    f
# gggg    ....    gggg    gggg    ....
#
#  5:      6:      7:      8:      9:
# aaaa    aaaa    aaaa    aaaa    aaaa
#b    .  b    .  .    c  b    c  b    c
#b    .  b    .  .    c  b    c  b    c
# dddd    dddd    ....    dddd    dddd
#.    f  e    f  .    f  e    f  .    f
#.    f  e    f  .    f  e    f  .    f
# gggg    gggg    ....    gggg    gggg

# 1, 3, 4, and 7 are easily identified by count of segments
# Where there are 6 and both match 1, that's a 9
# where there are 6 and 1 matches 1, that's a 6, plus we know slot 5/2
# If a 7 and we've completed 1, we can identify slot 0
# If an 8, then compare to 9 to get slot 4
# If an 8, then compare to 6 to get slot 2
# 
# 1. compare 1 and 7, that gives slot 0
# 2. compare 3 and 4, the one in 4 not in 3 is slot 1, the one in 3 and not 4 is slot 6 (excluding known slot 0)
# 3. compare 3/4 with 8, that gives slot 4
# 4. use 4, remove slot 1 and what's in 1, what's left is slot 3
#
#
# slots: XX2XX5X

# store what is in each section of single character display
display_map = ["", "", "", "", "", "", ""]
# store character sequence to number map
number_map = ["", "", "", "", "", "", ""]

input_file=open(sys.argv[1], "r")
unknowns = []
for input_value in input_file:
    input_value_line = input_value.strip()
    print input_value_line
    input_values = input_value.split("|")
    for value in input_values[1].split():
        if len(value) == 2:
            number_map[1] = value
        elif len(value) == 3:
            number_map[7] = value
        elif len(value) == 4:
            number_map[4] = value
        elif len(value) == 7:
            number_map[8]
        else:
            unknowns.append(value)
    break

print "Identified numbers"
print number_map
print "Unknown numbers"
print unknowns
print "Total: " + str(counter)

