#!/usr/bin/python
# Elves number game  
import sys
import math

#test_input = [0, 3, 6, 0]
test_input = [18, 8, 0, 5, 4, 1, 20, 0]

# If first time the number has been spoken, say 0
# Otherwise, how many turns apart the number is from the last time it was spoken

#last_spoken = {0:1, 3:2, 6:3}
last_spoken = {18:1, 8:2, 0:3, 5:4, 4:5, 1:6, 20:7}

for i in range(7,2019):
    current_value = test_input[i]
    print "Speaking " + str(current_value)
    
    #if i < len(test_input):
    #    last_spoken[current_value] = i + 1
    #    continue

    if last_spoken.has_key(current_value):
        test_input.append(i + 1 - last_spoken[current_value])
        print "Seen previously at index " + str(last_spoken[current_value]) 
    else:
        test_input.append(0)
        print "Haven't seen " + str(current_value) + " before"
    last_spoken[current_value] = i + 1 
    print test_input
