#!/usr/bin/python3
# Read file with int on each line for calories per item per elf. Blank line indicates separation between elves. 
import sys
import math

input_file=open(sys.argv[1], "r")
temp_elf = 0
elf_calories = []

for input_value in input_file:
    if input_value.strip() == "":
        elf_calories.append(temp_elf)
        temp_elf = 0
    else:
        temp_elf += int(input_value)

elf_calories.append(temp_elf)
elf_calories = sorted(elf_calories, reverse=True)
print(sum(elf_calories[0:3]))
#print(max(elf_calories))
