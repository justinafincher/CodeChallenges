#!/usr/bin/python
# Given elevation map, evaluation low points 
import sys
import math
import numpy

elevation_map = []

def return_adjacent_higher_points(point):
    points = []
    row = int(point.split(",")[0])
    column = int(point.split(",")[1])

    if elevation_map[row-1][column] > elevation_map[row][column] and int(elevation_map[row-1][column]) != 9:
        points.append(str(row-1) + "," + str(column))
    if elevation_map[row+1][column] > elevation_map[row][column] and int(elevation_map[row+1][column]) != 9:
        points.append(str(row+1) + "," + str(column))
    if elevation_map[row][column-1] > elevation_map[row][column] and int(elevation_map[row][column-1]) != 9:
        points.append(str(row) + "," + str(column-1))
    if elevation_map[row][column+1] > elevation_map[row][column] and int(elevation_map[row][column+1]) != 9:
        points.append(str(row) + "," + str(column+1))
    
    return points


def generate_basin_list(points_list, point):
    possible_additions = return_adjacent_higher_points(point)

    for new_point in possible_additions:
        if new_point in points_list:
            next
        else:
            points_list.append(new_point)
            points_list + generate_basin_list(points_list, new_point)

    return points_list

def distinct_list(list_of_points):
    new_list = []
    for point in list_of_points:
        if point not in new_list:
            new_list.append(point)

    return new_list

input_file=open(sys.argv[1], "r")
border = ""
first_line = 1
for input_value in input_file:
    input_value_line = input_value.strip()
    print input_value_line
    if first_line == 1:
        for i in range(0, len(input_value_line) + 2):
            border += "9"
        elevation_map.append(border)
        first_line = 0

    elevation_map.append(("9" + input_value_line + "9"))

elevation_map.append(border)

low_points = []
for row in range(1, len(elevation_map)-1):
    for column in range(1, len(elevation_map[row])-1):
        is_low_point = 1
        
        if elevation_map[row-1][column] <= elevation_map[row][column]:
            is_low_point = 0
        if elevation_map[row+1][column] <= elevation_map[row][column]:
            is_low_point = 0
        if elevation_map[row][column-1] <= elevation_map[row][column]:
            is_low_point = 0
        if elevation_map[row][column+1] <= elevation_map[row][column]:
            is_low_point = 0
        
        if is_low_point == 1:
            low_points.append(str(row)+","+str(column))

basin_sizes = []
for point in low_points:
    basin = []
    basin.append(point)
    basin += generate_basin_list(basin, point)
    basin_sizes.append(len(distinct_list(basin))) 

basin_sizes.sort(reverse=True)
score = 1
for i in range(0,3):
    score *= basin_sizes[i]

print score
