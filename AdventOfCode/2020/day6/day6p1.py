#!/usr/bin/python
# Sum counts of customs forms answer 

import sys
import math
import re

def calculate_group_score(group_answers):
    yes_questions = {}
    for i in range(0, len(group_answers)):
        yes_questions[group_answers[i]] = 1

    return sum(yes_questions.values())

input_file=open(sys.argv[1], "r")

cumulative_yesses = 0
group_answers = ""

for input_value in input_file:
    input_value = input_value.strip()

    # Blank line separates
    if len(input_value) == 0:
        cumulative_yesses += calculate_group_score(group_answers)
        group_answers = ""

    group_answers += input_value

# and the last one
cumulative_yesses += calculate_group_score(group_answers)
    
print cumulative_yesses
