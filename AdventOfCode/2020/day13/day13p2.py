#!/usr/bin/python
# Use bus schedule to determine when you can get to the airport  
import sys
import math

# First line of input is when you want to try to leave
# Second line is list of running busses (ignore x values)

input_file=open(sys.argv[1], "r")
departure_time = int(input_file.readline().strip())
temp_bus_schedule = input_file.readline().strip().split(",")

bus_intervals = []
# To have them align, we want t % bus_interval = bus_interval - place_in_line
# e.g. bus 1 (second bus) if interval 13, would have t % 13 = 12 to put the 2 % 13 = 0 at t + 1
bus_desired_remainder = []
# part of the CRT algorithm, product of all / number
product_array = []
bus_order = 0
bus_product = 1
print temp_bus_schedule
for bus in temp_bus_schedule:
    if bus != "x":
        bus_intervals.append(int(bus))
        
        if bus_order == 0:
            desired_remainder = 0
        elif bus_order == 1:
            desired_remainder = 1
        elif bus_order > bus:
            desired_remainder = bus_order % int(bus)
        else:
            desired_remainder = int(bus) - bus_order
        

        bus_desired_remainder.append(desired_remainder)
        bus_product = bus_product * int(bus)
    bus_order += 1

for bus in bus_intervals:
    product_array.append(bus_product / bus)

# Calculate muliplicative inverses
# (prod * x) % num = 1
inverses = []
for i in range(0, len(bus_intervals)):
    j = 1
    while 1:
        if (product_array[i] * j) % bus_intervals[i] == 1:
            inverses.append(j)
            break
        j += 1

print bus_intervals
print bus_desired_remainder
print bus_product
print product_array
print inverses

crt_calc_sum = 0
for i in range(0, len(bus_intervals)):
    crt_calc_sum += bus_desired_remainder[i] * product_array[i] * inverses[i]

print crt_calc_sum
print str(crt_calc_sum % bus_product)
