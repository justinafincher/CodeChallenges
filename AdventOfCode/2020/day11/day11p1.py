#!/usr/bin/python
# model loading of ferry based on seating chart 
import sys
import math
from collections import Counter

# Rules
# If a seat is empty (L) and there are no occupied seats adjacent to it, the seat becomes occupied.
# If a seat is occupied (#) and four or more seats adjacent to it are also occupied, the seat becomes empty.
# Otherwise, the seat's state does not change.
def update_seating_chart():
    # Loop through each "window" of seats that affect the rules
    for i in range(1, num_rows - 1):
        for j in range(1, num_cols - 1):
            # Skip floor seats
            if seating_chart[i][j] == ".":
                continue
            
            # Evaluate window at this position
            window = ""
            for k in range(i-1, i+2):
                for m in range(j-1, j+2):
                    window += seating_chart[k][m]
            window_counts = Counter(window)
            
            if seating_chart[i][j] == "L" and window_counts["#"] == 0:
                new_seating_chart[i][j] = "#"
            elif seating_chart[i][j] == "#" and window_counts["#"] > 4:
                new_seating_chart[i][j] = "L"
    return 

seating_chart = []
input_file=open(sys.argv[1], "r")

# if we pad the border with "floor" spaces, we can more consistent move an evaluation window around.
# This has already been done in the input file for top and bottom.
for input_value in input_file:
    input_value = "." + input_value.strip() + "."
    seating_chart.append(list(input_value))

num_rows = len(seating_chart)
num_cols = len(seating_chart[0])

print "Processing " + str(num_rows) + " rows and " + str(num_cols) + " columns"

new_seating_chart = []
first_loop = 1
while not seating_chart == new_seating_chart:
    if first_loop != 1:
#        print "copying new_seating_chart into seating_chart"
        seating_chart = []
        for row in new_seating_chart:
            seating_chart.append(list(row))
    else:
        new_seating_chart = []
        for row in seating_chart:
            new_seating_chart.append(list(row))
    first_loop = 0
    update_seating_chart()
#    print "Previous seating chart"
#    for row in seating_chart:
#        print row
#    print "New seating chart"
#    for row in new_seating_chart:
#        print row

occupied_seats = 0
for row in new_seating_chart:
    for column in row:
        if column == "#":
            occupied_seats += 1

print occupied_seats
