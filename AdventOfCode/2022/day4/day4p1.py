#!/usr/bin/python3
# Read file with int on each line for calories per item per elf. Blank line indicates separation between elves. 
import sys
import math

input_file=open(sys.argv[1], "r")
fully_contained_pairs = 0

for input_value in input_file:
    values = input_value.strip().split(",")
    #values = sorted(values)
    #print(values)
    if int(values[0].split("-")[0]) <= int(values[1].split("-")[0]) <= int(values[0].split("-")[1]) and \
        int(values[0].split("-")[0]) <= int(values[1].split("-")[1]) <= int(values[0].split("-")[1]): 
            fully_contained_pairs += 1
            #print(values)
            #print("contained")
    elif int(values[1].split("-")[0]) <= int(values[0].split("-")[0]) <= int(values[1].split("-")[1]) and \
        int(values[1].split("-")[0]) <= int(values[0].split("-")[1]) <= int(values[1].split("-")[1]): 
            fully_contained_pairs += 1
            #print(values)
            #print("contained")
    #else:
        #print(values)
        #print("not contained")

    #if values[0] == "6-14":
    #    print(str(values[1].split("-")[0]) + " <= " + str(values[0].split("-")[0]) + " <= " + str(values[1].split("-")[1]))
    #    print(int(values[1].split("-")[0]) <= int(values[0].split("-")[0]) <= int(values[1].split("-")[1]))
    #    print(values[1].split("-")[0] <= values[0].split("-")[1] <= values[1].split("-")[1])

print(fully_contained_pairs)
