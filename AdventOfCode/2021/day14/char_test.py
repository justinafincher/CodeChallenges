#!/usr/bin/python
# process polymer insertions 
import sys
import math
import numpy

pair_counts = {}
test_string = "NBBNBNBBCCNBCNCCNBBNBBNBBBNBBNBBCBHCBHHNHCBBCBHCB"

for i in range(1, len(test_string)):
    if test_string[i-1:i+1] in pair_counts.keys():
        pair_counts[test_string[i-1:i+1]] += 1
    else:
        pair_counts[test_string[i-1:i+1]] = 1

print pair_counts
