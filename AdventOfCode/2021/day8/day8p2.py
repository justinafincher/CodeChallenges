#!/usr/bin/python
#  
import sys
import math
import numpy
from collections import Counter
# display map
#  000
# 1   2
# 1   2
#  333
# 4   5
# 4   5
#  666

# Notes
#   0:      1:      2:      3:      4:
# aaaa    ....    aaaa    aaaa    ....
#b    c  .    c  .    c  .    c  b    c
#b    c  .    c  .    c  .    c  b    c
# ....    ....    dddd    dddd    dddd
#e    f  .    f  e    .  .    f  .    f
#e    f  .    f  e    .  .    f  .    f
# gggg    ....    gggg    gggg    ....
#
#  5:      6:      7:      8:      9:
# aaaa    aaaa    aaaa    aaaa    aaaa
#b    .  b    .  .    c  b    c  b    c
#b    .  b    .  .    c  b    c  b    c
# dddd    dddd    ....    dddd    dddd
#.    f  e    f  .    f  e    f  .    f
#.    f  e    f  .    f  e    f  .    f
# gggg    gggg    ....    gggg    gggg

# 1, 4, 7, and 8 are easily identified by count of segments
# Where there are 6 and both match 1, that's a 9 (NOPE because 8 also has 6 and matches)
# where there are 6 and 1 matches 1, that's a 6, plus we know slot 5/2
# If a 7 and we've completed 1, we can identify slot 0
# If an 8, then compare to 9 to get slot 4
# If an 8, then compare to 6 to get slot 2
# 
# 1. compare 1 and 7, that gives slot 0
# 2. compare 3 and 4, the one in 4 not in 3 is slot 1, the one in 3 and not 4 is slot 6 (excluding known slot 0)
# 3. compare 3/4 with 8, that gives slot 4
# 4. use 4, remove slot 1 and what's in 1, what's left is slot 3
#
#
# slots: XX2XX5X

# store what is in each section of single character display
display_map = ["", "", "", "", "", "", ""]
# store character sequence to number map
number_map = ["", "", "", "", "", "", "", "", "", ""]

input_file=open(sys.argv[1], "r")
unknowns = []
output_values = []
for input_value in input_file:
    input_value_line = input_value.strip()
    #print input_value_line
    input_values = input_value.split("|")
    for value in input_values[0].split():
        value = "".join(sorted(value))
        if len(value) == 2:
            number_map[1] = value
        elif len(value) == 3:
            number_map[7] = value
        elif len(value) == 4:
            number_map[4] = value
        elif len(value) == 7:
            number_map[8] = value
        else:
            unknowns.append(value)
    
    # knowns: (0), 1, 4, (6), 7, 8, (9)
    # 
    while len(unknowns) > 0:
        temp_unknowns = unknowns[:]
        # numbers with 6: 0, 6, 9
        # numbers with 5: 2, 3, 5 
        for value in unknowns:
            #print "Checking " + value
            if len(value) == 6:
                # If shares 1 character with 1, it's a 6
                if sum((Counter(value) & Counter(number_map[1])).values()) == 1:  
                    #print "Found string for 6"
                    number_map[6] = value
                    temp_unknowns.remove(value)
                # if shares 4 segments with 4, it's a 9
                elif sum((Counter(value) & Counter(number_map[4])).values()) == 4:
                    #print "Found string for 9"
                    number_map[9] = value
                    temp_unknowns.remove(value)
                # else it must be a 0
                else:
                    #print "Found string for 0"
                    number_map[0] = value
                    temp_unknowns.remove(value)
            elif len(value) == 5:
                # if it shares 2 segments with 1, it's a 3
                if sum((Counter(value) & Counter(number_map[1])).values()) == 2:
                    #print "Found string for 3"
                    number_map[3] = value
                    temp_unknowns.remove(value)
                # if it shares 2 segments with 4, it's a 2
                elif sum((Counter(value) & Counter(number_map[4])).values()) == 2:
                    #print "Found string for 2"
                    number_map[2] = value
                    temp_unknowns.remove(value)
                # else it is a 5
                else: 
                    #print "Found string for 5"
                    number_map[5] = value
                    temp_unknowns.remove(value)
            else:
                print "Unexpected value: " + value

        unknowns = temp_unknowns[:]

    # I don't want to redo what I did earlier, so just build a dictionary to map string->number
    value_map = {}
    for i in range(0, 10):
        value_map[number_map[i]] = i

    #print value_map
    
    output_string = ""
    for value in input_values[1].split():
        value = "".join(sorted(value))        
        output_string += str(value_map[value])

    print output_string
    output_values.append(int(output_string))

print "Total: " + str(sum(output_values))
