#!/usr/bin/python
# Password validation: Each line gives the password policy and then the password. The password 
# policy indicates the lowest and highest number of times a given letter must appear for the 
# password to be valid. For example, 1-3 a means that the password must contain a at least 1 time 
# and at most 3 times.
import sys
import math
import re

input_file=open(sys.argv[1], "r")

valid_passwords = 0

# Each line in form 1-4 a: abcde
# 1: min instances
# 2: max instances
# a: character to count instances
# abcde: example password
# Count the number of passwords that meet the associated requirement
for input_value in input_file:
    split_string = re.split('[- :]', input_value.strip())
    occurrences = split_string[4].count(split_string[2])
    #print "Checking if "+str(occurrences)+" is between "+split_string[0]+" and "+split_string[1]
    if occurrences >= int(split_string[0]) and occurrences <= int(split_string[1]):
        valid_passwords += 1
    
print valid_passwords
