#!/usr/bin/python
# Ransom note: Given magazine where full words are taken, can ransom note be built?
import sys

def ransom_note(magazine, ransom):
   mag_dict = {}
   for word in magazine:
      if word in mag_dict:
         mag_dict[word] += 1
      else:
         mag_dict[word] = 1
   
   for word in ransom:
      if word in mag_dict and mag_dict[word] > 0:
         mag_dict[word] -= 1
      else: 
         return 0

   return 1

m, n = map(int, raw_input().strip().split(' '))
magazine = raw_input().strip().split(' ')
ransom = raw_input().strip().split(' ')
answer = ransom_note(magazine, ransom)
if(answer):
   print "Yes"
else:
   print "No"
