#!/usr/bin/python3
# cube game 
import sys
import math


max_values = {
    "red" : 12,
    "green" : 13,
    "blue" : 14
}

input_file=open(sys.argv[1], "r")

id_sum = 0

for input_value in input_file:
    input_value = input_value.strip()
    header_values = input_value.split(":")
    game_id = header_values[0].split(" ")[1]
    #print(game_id)

    valid_game = True

    for cube_set in header_values[1].split(";"):
        #print("pull: "+cube_set)
        for cube in cube_set.split(","):
            cube_values = cube.strip().split(" ")
            #print(cube)
            if int(cube_values[0]) > max_values[cube_values[1]]:
                valid_game = False
        
    if valid_game:
        id_sum += int(game_id)

print(id_sum)
