#!/usr/bin/python3
# Tree visibility 
import sys
import math
import re

input_file=open(sys.argv[1], "r")

tree_grid = []


for input_value in input_file:
    input_value = input_value.strip()
    temp_row = []
    for i in range(0, len(input_value)):
        temp_row.append(input_value[i])
    tree_grid.append(temp_row.copy())

print(tree_grid)
rows = len(tree_grid)
columns = len(tree_grid[0])

print("Found "+str(rows)+" rows and "+str(columns)+" columns")

visible_trees = 2*rows + 2*(columns-2)

# track visibility: 0 invisible, 1 visible
tree_visibility_grid = []

for i in range(0, rows):
    temp_row = []
    if i == 0 or i == rows-1:
        for j in range(0, columns):
            temp_row.append(1) 
    else:
        for j in range(0, columns):
            if j == 0 or j == columns-1:
                temp_row.append(1)
            else:
                temp_row.append(0)
    tree_visibility_grid.append(temp_row.copy())

print('\n'.join(['\t'.join([str(cell) for cell in row]) for row in tree_grid]))
print("========")
print('\n'.join(['\t'.join([str(cell) for cell in row]) for row in tree_visibility_grid]))

# loop through inner grid of visibilities
# when looking through a row or column, track max height encountered. If current tree is greater
# than the tree is visible from that view. When evaluating visibility, don't update if already marked visible

# column down
print("Evaluating visibility for columns from top down")

for i in range(1, columns-1):
    max_height = tree_grid[0][i]
    for j in range(1, rows-1):
        if tree_grid[j][i] > max_height:
            max_height = tree_grid[j][i]
            tree_visibility_grid[j][i] = 1

print('\n'.join(['\t'.join([str(cell) for cell in row]) for row in tree_grid]))
print("========")
print('\n'.join(['\t'.join([str(cell) for cell in row]) for row in tree_visibility_grid]))
# row across right
print("Evaluating visibility for rows from left to right")

for i in range(1, rows-1):
    max_height = tree_grid[i][0]
    for j in range(1, columns-1):
        if tree_grid[i][j] > max_height:
            max_height = tree_grid[i][j]
            tree_visibility_grid[i][j] = 1

print('\n'.join(['\t'.join([str(cell) for cell in row]) for row in tree_grid]))
print("========")
print('\n'.join(['\t'.join([str(cell) for cell in row]) for row in tree_visibility_grid]))
# column up
print("Evaluating visibility for columns from bottom up")

for i in range(1, columns-1):
    max_height = tree_grid[rows-1][i]
    for j in range(rows-2, 0, -1):
        if tree_grid[j][i] > max_height:
            max_height = tree_grid[j][i]
            tree_visibility_grid[j][i] = 1

print('\n'.join(['\t'.join([str(cell) for cell in row]) for row in tree_grid]))
print("========")
print('\n'.join(['\t'.join([str(cell) for cell in row]) for row in tree_visibility_grid]))
# row across left
print("Evaluating visibility for rows from right to left")

for i in range(1, rows-1):
    max_height = tree_grid[i][columns-1]
    for j in range(columns-2, 0, -1):
        if tree_grid[i][j] > max_height:
            max_height = tree_grid[i][j]
            tree_visibility_grid[i][j] = 1

print('\n'.join(['\t'.join([str(cell) for cell in row]) for row in tree_grid]))
print("========")
print('\n'.join(['\t'.join([str(cell) for cell in row]) for row in tree_visibility_grid]))

print(tree_visibility_grid)
total_sum = 0
for row in tree_visibility_grid:
    total_sum += sum(row)
print(total_sum)

