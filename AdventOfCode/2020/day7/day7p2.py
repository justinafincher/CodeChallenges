#!/usr/bin/python
# Use bag descriptions to calculate possible container bags

# First modified the inputs with the following
#:%s/\([a-z]* [a-z]*\) bags contain /\1:/
#:%s/ bag[s]*, /:/g
#:%s/ bag[s]*.//
#:%s/\([0-9][0-9]*\) /\1|/g
#:%s/:no other//

import sys
import math
import re

input_file=open(sys.argv[1], "r")

# Given bag, return number of bags inside
def calculate_bags(bag_of_interest):
    if bags_seen.has_key(bag_of_interest):
        total_bags = 0
        # quick add the "parent" back counts
        for bag in bags_seen[bag_of_interest]:
            bag_parts = bag.split("|")
            num_bags = int(bag_parts[0])
            contained_bag = bag_parts[1]
            # Just add parent bags
            if bags_seen.has_key(contained_bag):
                total_bags += num_bags

        for bag in bags_seen[bag_of_interest]:
            bag_parts = bag.split("|")
            num_bags = int(bag_parts[0])
            contained_bag = bag_parts[1]
            
            if bags_seen.has_key(contained_bag):
                contained_bags = calculate_bags(contained_bag)
                total_bags += contained_bags * num_bags
            else:
                contained_bags = 1
                total_bags += contained_bags * num_bags
        
        return total_bags
    else:
        return 1

# Hold all bags seen
bags_seen = {}

interest_bag = "shiny gold"
contained_bags = 0

for input_value in input_file:
    input_value = input_value.strip()
    bag_infos = input_value.split(":")
    if len(bag_infos) == 1:
        continue
    current_bag = bag_infos[0]
    
    print current_bag
    
    del bag_infos[0]
    
    for bag in bag_infos:
        if bags_seen.has_key(current_bag):
            bags_seen[current_bag].append(bag)
        else:
            bags_seen[current_bag] = [bag]

print bags_seen
print calculate_bags(interest_bag)
