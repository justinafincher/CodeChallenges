#!/usr/bin/python
# Ferry navigation  
import sys
import math

def change_waypoint_position(direction, current_waypoint):
    temp_x = current_waypoint[0]
    temp_y = current_waypoint[1]
    if direction == "L":
        return [-temp_y, temp_x]
    elif direction == "R":
        return [temp_y, -temp_x]

# x, y
current_direction = "E"
current_ship_position = [0, 0]
current_waypoint = [10, 1]

# Action N means to move the waypoint north by the given value.
# Action S means to move the waypoint south by the given value.
# Action E means to move the waypoint east by the given value.
# Action W means to move the waypoint west by the given value.
# Action L means to rotate the waypoint around the ship left (counter-clockwise) the given number of degrees.
# Action R means to rotate the waypoint around the ship right (clockwise) the given number of degrees.
# Action F means to move forward to the waypoint a number of times equal to the given value.
input_file=open(sys.argv[1], "r")
for input_value in input_file:
    input_value = input_value.strip()

    direction_action = input_value[0]
    direction_distance = int(input_value[1:])
    print "Direction " + direction_action + " distance " + str(direction_distance)

    if direction_action == "F":
        for i in range(0, direction_distance):
            current_ship_position[0] += current_waypoint[0]
            current_ship_position[1] += current_waypoint[1]
    elif direction_action == "N":
        current_waypoint[1] += direction_distance
    elif direction_action == "S":
        current_waypoint[1] -= direction_distance
    elif direction_action == "E":
        current_waypoint[0] += direction_distance
    elif direction_action == "W":
        current_waypoint[0] -= direction_distance
    elif direction_action == "L" or direction_action == "R":
        for i in range(direction_distance / 90):
            current_waypoint = change_waypoint_position(direction_action, current_waypoint)

print str(abs(current_ship_position[0]) + abs(current_ship_position[1]))
