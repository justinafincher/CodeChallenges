#!/usr/bin/python
# Evaluate "joltage" adapter combinations 
import sys
import math

input_file=open(sys.argv[1], "r")

difference_distribution = {}
values = []

for input_value in input_file:
    input_value = input_value.strip()
    values.append(int(input_value))

values.sort()
previous_value = 0

# Device is always a difference of 3
difference_distribution[3] = 1

for value in values:
    difference = value - previous_value
    if difference_distribution.has_key(difference):
        difference_distribution[difference] += 1
    else:
        difference_distribution[difference] = 1

    previous_value = value

print difference_distribution
print difference_distribution[1] * difference_distribution[3] 
