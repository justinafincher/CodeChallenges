#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;

my $value1 = 2;
my $value2 = 5;

my @odd_values =  oddNumbers($value1,$value2);

my $res_size = @odd_values;

for(my $res_i=0;$res_i < $res_size; $res_i++){
   print "@odd_values[$res_i]\n";
}

sub oddNumbers {
    my $first = shift;
    my $second = shift;
    
    my $temp;

    if($first % 2 == 0){
        $temp = $first+1;
    }
    else{
        $temp = $first;
    }
   
    my @odd_values;
    while($temp <= $second){
        push(@odd_values,$temp);
        $temp+=2;
    }
    
    return @odd_values;
}

