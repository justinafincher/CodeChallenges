#!/usr/bin/python
# Do bubble sort, count swaps

n = int(raw_input().strip())
a = map(int, raw_input().strip().split(' '))

total_swaps = 0;

for i in range(0,n):
    # Track number of elements swapped during a single array traversal
    numberOfSwaps = 0
    
    for j in range(0,n-1):
        # Swap adjacent elements if they are in decreasing order
        if a[j] > a[j + 1]:
            temp = a[j+1]
            a[j+1] = a[j]
            a[j] = temp
            #swap(a[j], a[j + 1]);
            numberOfSwaps += 1
            total_swaps += 1
    
    # If no elements were swapped during a traversal, array is sorted
    if numberOfSwaps == 0:
        break

print "Array is sorted in "+str(total_swaps)+" swaps."
print "First Element: "+str(a[0])
print "Last Element: "+str(a[len(a)-1])
