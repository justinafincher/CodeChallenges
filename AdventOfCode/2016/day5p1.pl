use strict;
use warnings;
use Data::Dumper;
use Digest::MD5 qw(md5 md5_hex md5_base64);

my $input = "abbhdwsy";
#$input = "abc";

my $password;

my $suffix = 0;
#$suffix = 3231929;

for(my $i = 0; $i < 8; $i++) {
   my $full_string = $input.$suffix;
   my $digest = md5_hex($full_string);
   #print $digest."\n";
   #print substr($digest,5,1)."\n";
   #exit;
   while(substr($digest,0,5) ne "00000"){
      $suffix++;
      $full_string = $input.$suffix;
      $digest = md5_hex($full_string);
   }

   $suffix++;
   $password .= substr($digest,5,1);
   print "password is now |$password|\n";
   #exit;
}
   
print "Found password $password\n";
