use strict;
use warnings;
use Data::Dumper;

my $filename = 'day10input';
$filename = 'day10testinput';

open(my $fh, '<:encoding(UTF-8)', $filename)
  or die "Could not open file '$filename' $!";

my %instructions;
my $counter = 0;

while (my $row = <$fh>) {
   chomp $row;
  
   $instructions{$counter} = $row;
   $counter++;
}

my $total_instructions = keys %instructions;

print "Found $total_instructions instructions\n";

my %bots;
my $inst_counter = 0;
while(keys %instructions){
   next unless defined($instructions{$inst_counter});

   #print Dumper(%instructions);
   
   my @values = split(' ',$instructions{$inst_counter});
   

   delete $instructions{$inst_counter};
   $inst_counter++;

   $inst_counter = 0 if $inst_counter == $total_instructions;
}
