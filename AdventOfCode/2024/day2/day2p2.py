#!/usr/bin/python3
# classify reports as safe 
import sys
import math


# Return whether the report is safe, and if unsafe, include the index of the problematic value
def is_report_safe(report):
    is_increasing = True
    if int(report[0]) > int(report[1]):
        is_increasing = False

    for i in range(1, len(report)):
        current_num = int(report[i])
        previous_num = int(report[i-1])

        if previous_num == current_num:
            print(f"Report rejected: no increase or decrease | {previous_num} == {current_num}")
            return 0

        if (current_num > previous_num and is_increasing) or (current_num < previous_num and not is_increasing):
            if abs(current_num-previous_num) > 3:
                print(f"report rejected: change of greater than 3 | {previous_num} -> {current_num}")
                return 0
        elif (current_num < previous_num and is_increasing) or (current_num > previous_num and not is_increasing):
            print(f"report rejected: sequence changed direction | {is_increasing} | {previous_num} -> {current_num}")
            return 0
        
    return 1


def is_report_safe_wrapper(report):
    first_pass_safety = is_report_safe(report)

    if first_pass_safety == 1:
        return 1
    
    print(f"Report unsafe in {report}")

    for i in range(0,len(report)):
        patched_report = report.copy()
        patched_report.pop(i)
        if is_report_safe(patched_report) == 1:
            return 1

    return 0


input_file=open(sys.argv[1], "r")

safe_reports = 0

for input_value in input_file:
    input_value = input_value.strip()
    input_values = input_value.split()

    print(input_values)
    report_safety = is_report_safe_wrapper(input_values)
    print(report_safety)

    safe_reports += report_safety

print(f"Safe reports: {safe_reports}")
