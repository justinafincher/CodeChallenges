#!/usr/bin/python
# Navigate hyperthermal vents
import sys
import math
import numpy

def count_overlaps():
    count = 0
    for i in range(0, 1000):
        for j in range(0, 1000):
            if floor_map[i,j] > 1:
                count += 1
    return count

input_file=open(sys.argv[1], "r")

floor_map = numpy.zeros((1000, 1000)) 

for input_value in input_file:
    input_value = input_value.strip()
    input_values = input_value.split(" -> ")
    first_coord = input_values[0].split(",")
    second_coord = input_values[1].split(",")

    print input_values
    if first_coord[0] != second_coord[0] and first_coord[1] != second_coord[1]:
        print "Skipping diagonal line"
        next
    elif first_coord[0] != second_coord[0]:
        print "Processing x coord"
        for i in range(min(int(first_coord[0]), int(second_coord[0])), (max(int(first_coord[0]), int(second_coord[0]))+1)):
            print "updating locale " + str(i) + "," + str(first_coord[1])
            floor_map[i, int(first_coord[1])] += 1
    elif first_coord[1] != second_coord[1]:
        print "Processing y coord"
        for i in range(min(int(first_coord[1]), int(second_coord[1])), (max(int(first_coord[1]), int(second_coord[1]))+1)):
            print "updating locale " + str(first_coord[1]) + "," + str(i)
            floor_map[int(first_coord[0]), i] += 1

    print floor_map

print count_overlaps()
