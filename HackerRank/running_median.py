#!/usr/bin/python
import sys

def addValueToList(b,value,last_median):
   if last_median == 0:
      b.append(value)
      return b
   if value > last_median:
      for i in range(len(b)/2, len(b)):
         if b[i] > value:
            b.insert(i,value)
            return b
   else:
      for i in range(len(b)/2):
         if b[i] > value:
            b.insert(i,value)
            return b
   
   b.append(value)
   return b

def calculateMedian(b):
   b.sort() 
   midpoint = (len(b)-1)/2.0
   #print "Midpoint is "+str(midpoint)
   if len(b) == 1: 
      median =  float(b[0])
   elif midpoint % 1.0 == 0:
      median = float(b[int(midpoint)])
      #print "Has odd number of elements"
   else:
      first_index = int(midpoint-0.5)
      second_index = int(midpoint+0.5)
      median = (b[first_index]+b[second_index])/2.0

   print "%0.1f" % median
   return median

n = int(raw_input().strip())
a = []
a_i = 0
last_median = 0.0
for a_i in xrange(n):
    a_t = int(raw_input().strip())
    a = addValueToList(a,a_t,last_median)
    last_median = calculateMedian(a)
