#!/usr/bin/python3
# given directions, tell how many houses are visited, switching between santas
import sys
import math

def update_coords(coord_x, coord_y, move):
    if move == "<":
        coord_x -= 1
    elif move == ">":
        coord_x += 1
    elif move == "v":
        coord_y -= 1
    elif move == "^":
        coord_y += 1
    else:
        print(f"INVALID MOVE {move}")

    return coord_x, coord_y


input_file=open(sys.argv[1], "r")

is_santa = True

for input_value in input_file:
    santa_housing_coord_x = 0
    santa_housing_coord_y = 0
    robot_housing_coord_x = 0
    robot_housing_coord_y = 0
    houses_visited = {"0,0": 1}
    input_value = input_value.strip()
    for move in input_value:
        #print(f"move: {move}")
        if is_santa:
            santa_housing_coord_x, santa_housing_coord_y = update_coords(santa_housing_coord_x, santa_housing_coord_y, move)
            is_santa = False
            coord = str(santa_housing_coord_x) + "," + str(santa_housing_coord_y)
            #print(f"Santa moved to {coord}")
        else:
            robot_housing_coord_x, robot_housing_coord_y = update_coords(robot_housing_coord_x, robot_housing_coord_y, move)
            is_santa = True
            coord = str(robot_housing_coord_x) + "," + str(robot_housing_coord_y)
            #print(f"Robot moved to {coord}")
            
        if coord not in houses_visited.keys():
            houses_visited[coord] = 1
        #print(f"New coord: {housing_coord_x}, {housing_coord_y}")

    #print(houses_visited)
    print(f"Visited {sum(houses_visited.values())} houses")


