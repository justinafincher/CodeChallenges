#!/usr/bin/python
# model loading of ferry based on seating chart 
import sys
import math
from collections import Counter

# Rules
# Part 2 modification: View not adjacent, but first seat encountered in any of the 8 directions.
# If a seat is empty (L) and there are no occupied seats adjacent to it, the seat becomes occupied.
# If a seat is occupied (#) and five or more seats adjacent to it are also occupied, the seat becomes empty.
# Otherwise, the seat's state does not change.
def update_seating_chart():
    # create direction array to be able to have a single loop
    directions = [[-1,-1], [-1, 0], [-1, 1], [0, -1], [0, 1], [1, -1], [1, 0], [1, 1]] 
    # Loop through each seat to evaluate the rules
    for i in range(1, num_rows - 1):
        for j in range(1, num_cols - 1):
            # Skip floor seats
            if seating_chart[i][j] == ".":
                continue
            
            # Evaluate window at this position
            window = ""

            for direction in directions:
                temp_x = i + direction[0]
                temp_y = j + direction[1]
                while temp_x >= 0 and temp_x <= num_rows-1 and temp_y >= 0 and temp_y <= num_cols-1:
                    if seating_chart[temp_x][temp_y] != ".":
                        window += seating_chart[temp_x][temp_y]
                        break
                    temp_x = temp_x + direction[0]
                    temp_y = temp_y + direction[1]

            window_counts = Counter(window)
            
            if seating_chart[i][j] == "L" and window_counts["#"] == 0:
                new_seating_chart[i][j] = "#"
            elif seating_chart[i][j] == "#" and window_counts["#"] > 4:
                new_seating_chart[i][j] = "L"
    return 

seating_chart = []
input_file=open(sys.argv[1], "r")

# if we pad the border with "floor" spaces, we can more consistent move an evaluation window around.
# This has already been done in the input file for top and bottom.
for input_value in input_file:
    input_value = "." + input_value.strip() + "."
    seating_chart.append(list(input_value))

num_rows = len(seating_chart)
num_cols = len(seating_chart[0])

new_seating_chart = []
first_loop = 1
while not seating_chart == new_seating_chart:
    if first_loop != 1:
        seating_chart = []
        for row in new_seating_chart:
            seating_chart.append(list(row))
    else:
        new_seating_chart = []
        for row in seating_chart:
            new_seating_chart.append(list(row))
    first_loop = 0
    update_seating_chart()

occupied_seats = 0
for row in new_seating_chart:
    for column in row:
        if column == "#":
            occupied_seats += 1

print occupied_seats
