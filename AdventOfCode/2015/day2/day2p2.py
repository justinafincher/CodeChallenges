#!/usr/bin/python3
# calculate needed wrapping paper
from functools import reduce
from operator import mul
import sys
import math

input_file=open(sys.argv[1], "r")

total_ribbon_needed = 0

def calculate_ribbon_needed(dimensions):
    ribbon_needed = 0
    if len(dimensions) != 3:
        print("Wrong dimensions!")
        exit
    dimensions = [int(i) for i in dimensions]
    sorted_dimensions = sorted(dimensions)
    ribbon_needed += 2*sorted_dimensions[0] + 2*sorted_dimensions[1]
    ribbon_needed += reduce(mul, dimensions)
    return ribbon_needed


for input_value in input_file:
    input_value = input_value.strip()
    input_values = input_value.split("x")
    total_ribbon_needed += calculate_ribbon_needed(input_values)

print(total_ribbon_needed)

