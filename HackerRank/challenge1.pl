#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;

my @values = (0,1,2,3);
my $value = 3;

findNumber(\@values,$value);

sub findNumber {
    my $array = shift;
    my $value = shift;

    my $size = scalar @{$array};

    for(my $i = 0; $i < $size; $i++){
        if($value == $array->[$i]){
            print "YES";
            return;
        }
    }
    print "NO";
    return;
}

