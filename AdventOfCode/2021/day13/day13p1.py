#!/usr/bin/python
# fold transparent paper 
import sys
import math
import numpy

def distinct_list(list_of_points):
    new_list = []
    for point in list_of_points:
        if point not in new_list:
            new_list.append(point)

    return new_list


input_file=open(sys.argv[1], "r")

folding_instructions = []
holes = []
for input_value in input_file:
    input_value = input_value.strip()
    input_values = input_value.split(",")
    if len(input_values) > 1:
        #paper_sheet[int(input_values[1]),int(input_values[0])] = "#"
        holes.append(list(map(int, input_values)))
    elif len(input_value) > 0:
        input_value = input_value[11:]
        folding_instructions.append(input_value)

#print paper_sheet
print folding_instructions
print holes

# to calculate new hole locations, loop through the instructions and mod the values appropriately
for instruction in folding_instructions:
    instruction_parts = instruction.split("=")
    direction = instruction_parts[0]
    fold_line = int(instruction_parts[1])
    if direction == "y":
        for i in range(0, len(holes)):
            if holes[i][1] > fold_line:
                holes[i][1] = holes[i][1] - (2*(holes[i][1] - fold_line))
    elif direction == "x":
        for i in range(0, len(holes)):
            if holes[i][0] > fold_line:
                holes[i][0] = holes[i][0] - (2*(holes[i][0] - fold_line))
    else:
        print "Errant fold value"
        print instruction

    print "processed instruction"
    print instruction
    break

print holes
holes = distinct_list(holes)
print len(holes)
print len(distinct_list(holes))
# print out the sheet
#paper_sheet = numpy.full((15, 11),".")
#for hole in holes:
#    paper_sheet[int(hole[1]),int(hole[0])] = "#"
#print paper_sheet




