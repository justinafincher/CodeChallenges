
use strict;
use warnings;
use Scalar::Util qw(looks_like_number);
use Data::Dumper;

my $filename = $ARGV[0];

open IN, "$filename" or die "could not open $filename";

my $character;
my $total = 0;
my $temp = "";
my $othertemp = "";
my $lookingforred;
while(read(IN,$character,1)){
  
   if(looks_like_number($character) || $character eq "-"){
      $temp .= $character;
   }
   elsif(length($temp) > 0){
      print "Adding $temp\n";
      $total += ($temp + 0);
      $temp = "";
   }
   
   #last;
}

print $total;
