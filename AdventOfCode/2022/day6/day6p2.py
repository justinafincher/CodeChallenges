#!/usr/bin/python3
# Given crate stacks, calculate final orientation. 
import sys
import math
import re


input_file=open(sys.argv[1], "r")

# This puzzle has a single line input
with input_file as f:
    line = f.readline()

print(line)
for i in range(14, len(line)):
    #print(line[i-4:i])
    if(len(set(line[i-14:i])) == 14):
        print("Found start at position "+str(i))
        break
