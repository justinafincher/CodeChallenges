#!/usr/bin/python3
# Given rock paper scissors guide, calculate score 
import sys
import math

input_file=open(sys.argv[1], "r")

# values for each play choice
play_scores = {
    "X":1,
    "Y":2,
    "Z":3
}

beat_by = {
    "A":"Y",
    "B":"Z",
    "C":"X"
}

play_translation = {
    "A":"X",
    "B":"Y",
    "C":"Z"
}

total_score = 0

for input_value in input_file:
    round_score = 0
    values = input_value.strip().split(" ")
    print(values)
    round_score += play_scores[values[1]]

    # draw
    if play_translation[values[0]] == values[1]:
        round_score += 3
    elif beat_by[values[0]] == values[1]:
        round_score += 6
    else:
        round_score += 0

    total_score += round_score

print("Total score: "+str(total_score))
#elf_calories.append(temp_elf)

#print(elf_calories)
#print(max(elf_calories))
