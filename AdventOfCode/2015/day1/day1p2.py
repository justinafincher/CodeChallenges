#!/usr/bin/python3
# parens calculate ending floor 
import sys
import math

input_file=open(sys.argv[1], "r")


def find_basement(input):
    floor = 0
    position = 1
    for character in input:
        if character == "(":
            floor += 1
        else:
            floor -= 1

        if floor < 0:
            return position

        position += 1


for input_value in input_file:
    input_value = input_value.strip()
    print(f"Basement position: {find_basement(input_value)}")

