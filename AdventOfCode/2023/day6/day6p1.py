#!/usr/bin/python3
# Calculate number of ways to win boat races 
import sys
import math

def calculate_distance(charge_time, total_time):
    return (int(total_time)-int(charge_time))*int(charge_time)

input_file=open(sys.argv[1], "r")

ways_to_win = 1

times = []
distances = []

for input_value in input_file:
    input_value = input_value.strip()
    input_values = input_value.split()
    if input_values[0] == "Time:":
        times = [int(i) for i in input_value.split()[1:]]
    elif input_values[0] == "Distance:":
        distances = [int(i) for i in input_value.split()[1:]]
    else:
        print("Unexpected input: "+input_value)

print(times)
print(distances)

# Let's try brute force
for i in range(0, len(times)):
    win_counter = 0
    for j in range(0, int(times[i])+1):
        if distances[i] < calculate_distance(j, times[i]):
            win_counter += 1
    if win_counter > 0:
        ways_to_win *= win_counter

print(ways_to_win)
