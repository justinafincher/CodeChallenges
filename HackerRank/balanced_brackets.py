#!/usr/bin/python
# For each line, evaluate whether parens/brackets/braces are balanced

def is_matched(expression):
   balance_tracker = []
   for char in expression:
      char = char.strip() 
      if char == ')':
         if len(balance_tracker) > 0:
            other_char = balance_tracker.pop()
            if other_char != '(':
               return False
         else:
            return False
      elif char == ']':
         if len(balance_tracker) > 0:
            other_char = balance_tracker.pop()
            if other_char != '[':
               return False
         else:
            return False
      elif char == '}':
         if len(balance_tracker) > 0:
            other_char = balance_tracker.pop()
            if other_char != '{':
               return False
         else:
            return False
      else:
         balance_tracker.append(char)
   
   if len(balance_tracker) != 0:
      return False
   else:
      return True

t = int(raw_input().strip())
for a0 in xrange(t):
    expression = raw_input().strip()
    if is_matched(expression) == True:
        print "YES"
    else:
        print "NO"
