#!/usr/bin/python3
# Word search, find all XMAS 
import sys
import re

input_file=open(sys.argv[1], "r")

word_to_find = "XMAS"

word_found_count = 0

original_word_search = []

def horizontal_stringify(word_search):    
    strings = ""
    for i in range(0,len(word_search)):
        strings += "".join(word_search[i])
        strings += " "

    return strings


def vertical_stringify(word_search):
    strings = ""

    for i in range(0,len(word_search[0])):
        for j in range(0,len(word_search)):
            strings += word_search[j][i]
        strings += " "

    return strings


def diagonal_stringify(word_search):
    strings = ""

    total_rows = len(word_search)
    total_columns = len(word_search[0])

    # start with top left to bottom right diags
    column = 0
    for row in range(len(word_search)-len(word_to_find), -1, -1):
        temp_string = ""
        #row = len(word_search)-len(word_to_find)
    
        for i in range(0, total_rows):
            if row+i < total_rows and column+i < total_columns:
                temp_string += word_search[row+i][column+i]
        print(temp_string)

        strings += temp_string
        strings += " "

    row = 0
    for column in range(1, total_columns-len(word_to_find)+1):
        temp_string = ""
        #row = len(word_search)-len(word_to_find)
    
        for i in range(0, total_rows):
            if row+i < total_rows and column+i < total_columns:
                temp_string += word_search[row+i][column+i]
        print(temp_string)

        strings += temp_string
        strings += " "

    # Now diagonals that go bottom left to top right
    column = 0
    for row in range(len(word_to_find)-1, total_rows):
        temp_string = ""
        #row = len(word_search)-len(word_to_find)
    
        for i in range(0, total_columns):
            #print(f"Checking row {row-i} and column {column+i}")
            if row-i >=0 and column+i < total_columns:
                temp_string += word_search[row-i][column+i]
        print(temp_string)

        strings += temp_string
        strings += " "

    row = total_rows-1
    for column in range(1, total_columns-len(word_to_find)):
        temp_string = ""
        #row = len(word_search)-len(word_to_find)
    
        for i in range(0, total_rows):
            #print(f"Checking row {row-i} and column {column+i}")
            if row-i >= 0 and column+i < total_columns:
                temp_string += word_search[row-i][column+i]
            else:
                break
        print(temp_string)

        strings += temp_string
        strings += " "

    return strings


def stringify_word_search(word_search):
    return horizontal_stringify(word_search) + vertical_stringify(word_search) + diagonal_stringify(word_search)  


for input_value in input_file:
    input_value = input_value.strip()

    row_values = []
    
    for i in range(0,len(input_value)):
        row_values.append(input_value[i])

    original_word_search.append(row_values)

word_search_strings = stringify_word_search(original_word_search)

print(f"Forward strings: {word_search_strings.count(word_to_find)}")
print(f"Reverse strings: {word_search_strings.count(word_to_find[::-1])}")

#print(original_word_search)
    
#print(horizontal_stringify(original_word_search))
#print("-----")
#print(vertical_stringify(original_word_search))
#print("-----")
#print(diagonal_stringify(original_word_search))
