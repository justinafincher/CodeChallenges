use strict;
use warnings;
use Data::Dumper;
use Digest::MD5 qw(md5 md5_hex md5_base64);
use Scalar::Util qw(looks_like_number);

my $input = "abbhdwsy";
#$input = "abc";

my @password = ("-","-","-","-","-","-","-","-");

my $suffix = 0;
#$suffix = 3231929;

for(my $i = 0; $i < 8; $i++) {
   my $full_string = $input.$suffix;
   my $digest = md5_hex($full_string);
   #print $digest."\n";
   #print substr($digest,5,1)."\n";
   #exit;
   while(1){

      if(substr($digest,0,5) eq "00000"){
         my $position = substr($digest,5,1);
         #print "Correct hash, position: $position\n";
         #print "Replacing ".$password[$position]." with new value\n";
         if (looks_like_number($position) && $position >= 0 && $position <= 7 && $password[$position] eq "-"){
            $password[$position] = substr($digest,6,1);
            last;
         }
         else{
            #print "Not valid or spot taken\n";
            $suffix++;
            $full_string = $input.$suffix;
            $digest = md5_hex($full_string);
            next;
         }
      } 
      else{
         $suffix++;
         $full_string = $input.$suffix;
         $digest = md5_hex($full_string);
      } 
   }

   $suffix++;

   print "password is now |".join('',@password)."|\n";
   #print Dumper(@password);
   #exit;
}
   
print "Found password ".join('',@password)."\n";
