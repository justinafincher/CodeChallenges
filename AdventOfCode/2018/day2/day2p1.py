#!/usr/bin/python
# Read file with signed int on each line. Calculate sum.

input_file=open("day2input.txt", "r")

two_repeat_count = 0
three_repeat_count = 0

for line in input_file:
    found_two = 0
    found_three = 0

    temp_char_counts = {}
    characters = list(line)
    
    for character in characters:
        if character in temp_char_counts:
            temp_char_counts[character] += 1
        else:
            temp_char_counts[character] = 1
    
    for character in temp_char_counts:
        if temp_char_counts[character] == 2 and found_two == 0:
            two_repeat_count += 1
            found_two = 1
        elif temp_char_counts[character] == 3 and found_three == 0:
            three_repeat_count += 1
            found_three = 1

print "two repeat count: "+str(two_repeat_count)
print "three repeat count: "+str(three_repeat_count)
print "checksum: "+str(two_repeat_count*three_repeat_count)
