#!/usr/bin/python
# simulate lanternfish growth
# given timer list of lantern fish, simulate population over cycles. 
# Each resets to 6 after 0, but newly spawned are set to 8
import sys
import math
import numpy

def update_population(population):
    new_population = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    
    for i in range(0, len(population)):
        # for population with timer 0, move all to 6, but also create new at 8
        if i == 0:
            new_population[6] += population[i]
            new_population[8] += population[i]
        else:
            new_population[i-1] += population[i]
        #print "Population after single update"
        #print new_population
        
    return new_population

input_file=open(sys.argv[1], "r")

# keep track of a count of laternfish at each timer level
population = [0, 0, 0, 0, 0, 0, 0, 0, 0]

# just leaving the loop even though this one is a one line input
for input_value in input_file:
    input_value_line = input_value.strip()
    input_values = input_value.split(",")
    for value in input_values:
        #print "Value: " + value
        population[int(value)] += 1

#print population

for i in range(0, 256):
    population = update_population(population)
    print "Population updated"
    print population
    print sum(population)

print "Total fish: " + str(sum(population))
