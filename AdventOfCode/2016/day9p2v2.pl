use strict;
use warnings;
use Data::Dumper;

my $filename = 'day9input';
#$filename = 'day9testinput2a';
#$filename = 'day9testinput2b';
#$filename = 'day9testinput2c';
#$filename = 'day9testinput2d';

open(my $fh, '<:encoding(UTF-8)', $filename)
  or die "Could not open file '$filename' $!";

my $total_char_count = 0;
my $character;

read $fh, $character, 1;
MAIN_LOOP:
while ($character ne "") {
   my $count = 0;

   if($character eq "("){
      $count = decompress_from_file($fh); 
   }
   elsif($character ne "\n"){
      $count = 1;
   }
  
   #print "Adding $count to total after processing |$character|\n";

   $total_char_count += $count;

   read $fh, $character, 1;
}

print "Decompressed string is $total_char_count characters long\n";

# continued decompression indicates we are just decompressing a string instead of reading from a file.
sub decompress_from_file {
   my $in_file_handle = shift;

   my $character = "";
   my $repeat_instructions = "";
   my $count = 0;
   
   read $in_file_handle, $character, 1;
   while($character ne ")"){
      $repeat_instructions .= $character;
      read $in_file_handle, $character, 1;
   }
   #print "Found repeat instructions $repeat_instructions\n";
   my @repeat_instructions = split('x',$repeat_instructions);
   my $num_letters = $repeat_instructions[0];
   my $repeats = $repeat_instructions[1];

   my $repeat_string = "";
   my $new_char;
   for(my $i = 0; $i < $num_letters; $i++){
      read $in_file_handle, $new_char, 1;
      $repeat_string .= $new_char;
   }

   my $temp_expanded_string = "";
   for(my $i = 0; $i < $repeats; $i++){
      $temp_expanded_string .= $repeat_string;
   }

   if(index($temp_expanded_string,"(") >= 0){
      #print "Still need expanding of |$temp_expanded_string|\n";
      $count = decompress_string($temp_expanded_string);
   }
   else{
      $count += length($temp_expanded_string);
   }

   return $count;
}

sub decompress_string {
   # store strings as within and outside brackets
   my $string = shift;
   my @values = split('',$string);

   print localtime."\tRunning additional decompression of string starting with ".$values[0].$values[1].$values[2].$values[3].$values[4]."\n";
   #print localtime."\tRunning additional decompression of string $string\n";
   my $count = 0;
   my $decompressed_string = '';

   for(my $i = 0;  $i < @values; $i++) {
      if($values[$i] eq '('){
         my $repeat_instructions;
         my $counter = $i+1;
         while($values[$counter] ne ")"){
            $repeat_instructions .= $values[$counter];
            $counter++;
         }
         my @repeat_instructions = split('x',$repeat_instructions);
         my $num_letters = $repeat_instructions[0];
         my $repeats = $repeat_instructions[1];
  
         my $base_index = $counter+1;

         my $repeat_string;
         for(my $j = 0; $j < $num_letters; $j++){
            $repeat_string .= $values[$base_index+$j];
         }

         #print "Repeating string $repeat_string for $repeats times\n";

         if(index($repeat_string,"(") >= 0) {
            my $temp_count = decompress_string($repeat_string);
            $temp_count = $temp_count * $repeats;
            $count += $temp_count;
         }
         else{
            $count += ($repeats * $num_letters);
         }
         #print "Repeating the next $num_letters for $repeats times\n";
         #print "Found repeat string of $repeat_string\n";
         #for(my $j = 0; $j < $repeats; $j++){
         #   $decompressed_string .= $repeat_string;
         #}
         $i = $base_index+$num_letters-1;
         #print "Added repeats to string: $decompressed_string resetting index to $i which points at ".$values[$i]."\n";
         #next;
      }
      else {
         $count++;
         #$decompressed_string .= $values[$i];
      }
   }

   #if(index($decompressed_string,"(") >= 0){
   #   $count += decompress_string($decompressed_string);
   #}
   #else{
   #   $count += length($decompressed_string);
   #}
   
   return $count;
}
      #last;
   #print "Decompressed string is of length ".length($temp_string)."\n";
