#!/usr/bin/python
# Read file with int on each line. Divide by 3, round down, then subtract 2.
import sys
import math

def truncate(n, decimals=0):
    multiplier = 10 ** decimals
    return int(n * multiplier) / multiplier

def calculate_fuel_requirement(n):
    fuel = truncate(n/3)-2
    if fuel > 0:
        return fuel
    else:
        return 0

input_file=open(sys.argv[1], "r")

total_fuel_requirement = 0

for input_value in input_file:
    fuel_for_this_module = 0 
    fuel_requirement = calculate_fuel_requirement(int(input_value))
    total_fuel_requirement += fuel_requirement
    fuel_for_this_module += fuel_requirement

    while(fuel_requirement > 0):
        fuel_requirement = calculate_fuel_requirement(fuel_requirement)
        total_fuel_requirement += fuel_requirement
        fuel_for_this_module += fuel_requirement

print "Total fuel requirement: "+str(total_fuel_requirement)
