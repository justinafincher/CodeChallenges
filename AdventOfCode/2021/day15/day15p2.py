#!/usr/bin/python
# find path with lowest risk
import sys
import math
import numpy
numpy.set_printoptions(threshold=sys.maxsize)
numpy.set_printoptions(linewidth=12000)

def distinct_list(list_of_points):
    new_list = []
    for point in list_of_points:
        if point not in new_list:
            new_list.append(point)
        else:
            print "Found duplicate: "+point

    return new_list

input_file=open(sys.argv[1], "r")

cave_costs_temp = []

for input_value in input_file:
    input_value = input_value.strip()
    cave_costs_temp.append(input_value)

rows = len(cave_costs_temp)
columns = len(cave_costs_temp[0])
# convert cave_costs_temp into numpy array
cave_costs = numpy.full((rows*5, columns*5), 0)

# first copy into array
for i in range(0, rows*5):
    for j in range(0, columns*5):
        if i < rows and j < columns:
            cave_costs[i,j] = int(cave_costs_temp[i][j])
# now fill out rest of the array
for i in range(0, rows*5):
    for j in range(0, columns*5):
        if i < rows and j < columns:
            next
        elif i < rows:
            cave_costs[i,j] = int(cave_costs[i,j-columns]) % 9 + 1
        else:# j < columns:
            cave_costs[i,j] = int(cave_costs[i-rows,j]) % 9 + 1
        #else:
        #    cave_costs[i,j] = int(cave_costs[i-rows,j-columns]) % 9 + 1


print cave_costs
full_rows = rows*5
full_columns = columns*5
risks = numpy.full((full_rows, full_columns),1000000)
risks[0,0] = 0

# djikstra. Track visited and unvisited nodes.
unvisited_neighbors = []
visited_nodes = []

test_iterations = 0

active_node = "0,0"
while True:    
    #print "Evaluating active node: "+active_node
    #if active_node == "49,49":
    #    break

    # consider unvisited neighbors
    row = int(active_node.split(",")[0])
    column = int(active_node.split(",")[1])
    
    # update neighbors
    # left -> row, column-1
    if column-1 >= 0:
        coord = str(row)+","+str(column-1)
        if coord not in visited_nodes and coord not in unvisited_neighbors:
            unvisited_neighbors.append(str(row)+","+str(column-1))
        #if cave_costs[row,column-1] < minimum_neighbor_value and str(row)+","+str(column-1) not in visited_nodes:
        #    minimum_neighbor_value = cave_costs[row,column-1]
        #    minimum_neighbor = str(row)+","+str(column-1)
        if coord not in visited_nodes and (risks[row,column] + cave_costs[row,column-1]) < risks[row,column-1]:
            risks[row,column-1] = risks[row,column] + cave_costs[row,column-1]
    # up -> row-1, column
    if row-1 >= 0:
        coord = str(row-1)+","+str(column)
        if coord not in visited_nodes and coord not in unvisited_neighbors:
            unvisited_neighbors.append(coord)
        #if cave_costs[row-1,column] < minimum_neighbor_value and str(row-1)+","+str(column) not in visited_nodes:
        #    minimum_neighbor_value = cave_costs[row-1,column]
        #    minimum_neighbor = str(row-1)+","+str(column)
        if coord not in visited_nodes and (risks[row,column] + cave_costs[row-1,column]) < risks[row-1,column]:
            risks[row-1,column] = risks[row,column] + cave_costs[row-1,column]
    # right -> row, column+1
    if column+1 < full_columns:
        coord = str(row)+","+str(column+1)
        if coord not in visited_nodes and coord not in unvisited_neighbors:
            unvisited_neighbors.append(coord)
        #if cave_costs[row,column+1] < minimum_neighbor_value and str(row)+","+str(column+1):
        #    minimum_neighbor_value = cave_costs[row,column+1]
        #    minimum_neighbor = str(row)+","+str(column+1)
        if coord not in visited_nodes and (risks[row,column] + cave_costs[row,column+1]) < risks[row,column+1]:
            risks[row,column+1] = risks[row,column] + cave_costs[row,column+1]
    # down -> row+1, column
    if row+1 < full_rows:
        coord = str(row+1)+","+str(column)
        if coord not in visited_nodes and coord not in unvisited_neighbors:
            unvisited_neighbors.append(coord)
        #if cave_costs[row+1,column] < minimum_neighbor_value and str(row+1)+","+str(column):
        #    minimum_neighbor_value = cave_costs[row+1,column]
        #    minimum_neighbor = str(row+1)+","+str(column)
        if coord not in visited_nodes and (risks[row,column] + cave_costs[row+1,column]) < risks[row+1,column]:
            risks[row+1,column] = risks[row,column] + cave_costs[row+1,column]

    # choose next vertex by choosing minimum neighbor
    visited_nodes.append(active_node)
    
    minimum_neighbor_value = 100000
    minimum_neighbor = ""
    #print "Checking neighbors"
    #print unvisited_neighbors
    for neighbor in unvisited_neighbors:
        #print "Evaluating neighbor: "+neighbor+ " with cost "+str(cave_costs[int(neighbor.split(",")[0]),int(neighbor.split(",")[1])])
        #print "Evaluating neighbor: "+neighbor+ " with cumulative cost "+str(risks[int(neighbor.split(",")[0]),int(neighbor.split(",")[1])])
        if risks[int(neighbor.split(",")[0]),int(neighbor.split(",")[1])] < minimum_neighbor_value:
            minimum_neighbor_value = risks[int(neighbor.split(",")[0]),int(neighbor.split(",")[1])]
            minimum_neighbor = neighbor

    #if len(visited_nodes) == full_rows*full_columns or active_node == str(full_rows)+","+str(full_columns):
    if active_node == str(full_rows-1)+","+str(full_columns-1):
        break
   
    #print "Neighbor "+minimum_neighbor+" chosen from list"

    active_node = minimum_neighbor
    unvisited_neighbors.remove(active_node)

    print "Unvisited neighbors: " + str(len(unvisited_neighbors))
    print "Visited nodes: " + str(len(visited_nodes))
    
    #print all_neighbors
    visited_length = len(visited_nodes)
    deduped_visited_length = len(distinct_list(visited_nodes))
    if visited_length != deduped_visited_length:
        break
    #test_iterations += 1
    #if test_iterations >= 40:
    #    break

print risks
print visited_nodes
#print len(visited_nodes)
#print "Expected visited_length = "+str(full_rows*full_columns)
