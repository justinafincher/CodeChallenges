#!/usr/bin/python
# Read file with set of code on each line
# opcode position1 position2 outputposition
# opcode 1 addition
# opcode 2 multiplication
# opcode 99 exit

# Find values in positions 1 and 2 that result in a value of 19690720 in position 0
import sys

input_file=open(sys.argv[1], "r")

for line in input_file:
    line = line.rstrip('\n') 
    original_line_elements = line.split(',')

    for code1 in range(100):
        for code2 in range(100):
            #print "Testing with values "+str(code1)+" and "+str(code2)
            
            opcode_index = 0
            line_elements = list(original_line_elements)
            line_elements[1] = code1
            line_elements[2] = code2

            while 1:
                opcode = int(line_elements[opcode_index])
                if opcode == 99:
                    break
                position_1 = int(line_elements[opcode_index+1])
                position_2 = int(line_elements[opcode_index+2])
                output_position = int(line_elements[opcode_index+3])

                #print "Executing opcode "+str(opcode)+" reading from positions "+str(position_1)+" and "+str(position_2)+" to save in "+str(output_position)
                if opcode == 1:
                    line_elements[output_position] = int(line_elements[position_1]) + int(line_elements[position_2])
                if opcode == 2:
                    line_elements[output_position] = int(line_elements[position_1]) * int(line_elements[position_2])

                opcode_index += 4

            if str(line_elements[0]) == "19690720":
                print "Found value 19690720 with inputs "+str(code1)+" and "+str(code2)
                sys.exit()
