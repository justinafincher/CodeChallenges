
# Creates a list containing 5 lists, each of 8 items, all set to 0
test = 0
display_width = 50 
display_height = 6
if test == 1:
   display_width = 7 
   display_height = 3

display = [['.' for x in range(display_width)] for y in range(display_height)] 

filename = "day8input"
if test == 1:
   filename = "day8testinput"

def print_display(display):
   print('\n'.join([''.join(['{:1}'.format(item) for item in row]) for row in display]))

def add_rect(display, width, height):
   for i in range(height):
      for j in range(width):
         display[i][j] = '#'

def rotate_column(display, column, amount):
   for i in range(amount):
      # rotate the column one time
      temp = display[display_height-1][column]
      for j in range(display_height-1,0,-1):
         #print "Setting row "+str(j)+" to value in row "+str(j-1)+"("+str(display[j-1][column])+") in column "+str(column)
         display[j][column] = display[j-1][column]
      display[0][column] = temp

def rotate_row(display, row, amount):
   for i in range(amount):
      # rotate the row one time
      temp = display[row][display_width-1]
      for j in range(display_width-1,0,-1):
         #print "Setting row "+str(j)+" to value in row "+str(j-1)+"("+str(display[j-1][column])+") in column "+str(column)
         display[row][j] = display[row][j-1]
      display[row][0] = temp

def count_leds(display):
   counter = 0
   for i in range(display_height):
      for j in range(display_width):
         if display[i][j] == "#":
            counter += 1
   return counter

f = open(filename, 'r')
line = f.readline().strip('\n')
while line:
   print line
   line_values = line.split(' ')
   #print line_values
   if line_values[0] == "rect":
      dimensions = line_values[1].split("x")
      print "Adding rectangle with dimensions "+dimensions[0]+" by "+dimensions[1]
      add_rect(display,int(dimensions[0]),int(dimensions[1]))
   elif line_values[1] == "column":
      other_values = line_values[2].split('=')
      column = other_values[1]
      shift = line_values[4]
      print "Shifting column "+column+" by "+shift
      rotate_column(display, int(column), int(shift));
   elif line_values[1] == "row":
      other_values = line_values[2].split('=')
      row = other_values[1]
      shift = line_values[4]
      print "Shifting row "+row+" by "+shift
      rotate_row(display, int(row), int(shift))
      
   print_display(display)

   line = f.readline().strip('\n');

print count_leds(display)

