#!/usr/bin/python3
# find md5 hashes
import sys
import math
import hashlib

input_file=open(sys.argv[1], "r")

string_prefix = "000000"

for input_value in input_file:
    input_value = input_value.strip()
    
    counter = 0
    while(1):
        #print(f"Testing {counter}")
        test_string = input_value + str(counter)
        hashed_string = hashlib.md5(test_string.encode()).hexdigest()
        if hashed_string.startswith(string_prefix):
            print(f"Found md5 of {test_string}: {hashed_string}")
            break
        counter += 1
