#!/usr/bin/python
# Read file with int on each line. Calculate number of times value increases. 
import sys
import math

input_file=open(sys.argv[1], "r")

previous_value = 0
number_of_increases = -1 # super janky to revert initial "increase"

for input_value in input_file:
    if int(input_value) > previous_value:
        print "Found increase from " + str(previous_value) + " to " + str(input_value)
        number_of_increases += 1
    else:
        print "No increase from " + str(previous_value) + " to " + str(input_value)
    previous_value = int(input_value)

print "Found " + str(number_of_increases) + " increases in depth"
