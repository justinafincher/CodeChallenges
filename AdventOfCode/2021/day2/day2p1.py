#!/usr/bin/python
# Given instructions, figure out where the submarine is
import sys
import math

input_file=open(sys.argv[1], "r")

position = 0
depth = 0

for input_value in input_file:
    directions = input_value.split(" ")
    if directions[0] == "forward":
        position += int(directions[1])
    elif directions[0] == "up":
        depth -= int(directions[1])
    elif directions[0] == "down":
        depth += int(directions[1])
    else:
        print "ERRANT VALUE FOUND " + directions[0]

print "Position: " + str(position)
print "Depth: " + str(depth)
print "Result: " + str(position * depth)
