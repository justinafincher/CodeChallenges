#!/usr/bin/python
# Read file with signed int on each line. Calculate sum.
import sys

input_file=open("day1input.txt", "r")

sum = 0
previous_sums = []

while(1):
    input_file=open("day1input.txt", "r")
    
    for line in input_file:
        sum += int(line)
        print sum
        if sum in previous_sums:
            print "Sum encountered second time: "+str(sum)
            sys.exit(1)
        previous_sums.append(sum)
    
    input_file.close()
