#!/usr/bin/python
# Find program value before infinite loop 

import sys
import math
import re

input_file=open(sys.argv[1], "r")

accumulator = 0
instruction_index = 0
instruction_tracker = {}
instructions = []
temp_values = []

for input_value in input_file:
    input_value = input_value.strip()
    instructions.append(input_value)

while 1:
    input_values = instructions[instruction_index].split(' ')
    instruction = input_values[0]
    argument = int(input_values[1])

    # Exit if trying to repeat an instruction
    if instruction_tracker.has_key(instruction_index):
        break

    instruction_tracker[instruction_index] = 1
    
    if instruction == "nop":
        instruction_index += 1
    if instruction == "acc":
        accumulator += argument
        instruction_index += 1
    if instruction == "jmp":
        instruction_index += argument

print accumulator
