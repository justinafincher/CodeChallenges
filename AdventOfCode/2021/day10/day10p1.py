#!/usr/bin/python
# Process corrupted navigational input 
import sys
import math
import numpy

error_scoring = {
    ")":3,
    "]":57,
    "}":1197,
    ">":25137
}

input_file=open(sys.argv[1], "r")

chunk_definitions = {
    "(":")",
    "[":"]",
    "{":"}",
    "<":">"
}

invalid_characters = []

for input_value in input_file:
    input_value_line = input_value.strip()
    print input_value_line
    checking_array = []
    for i in range(0, len(input_value_line)):
        if input_value_line[i] in chunk_definitions.keys():
            checking_array.append(input_value_line[i])
        elif input_value_line[i] in chunk_definitions.values():
            if chunk_definitions[checking_array[-1]] == input_value_line[i]:
                checking_array.pop()
            else:
                print "Invalid character: "+input_value_line[i]
                invalid_characters.append(input_value_line[i])
                break
        else:
            print "Unknown character found: "+input_value_line[i]

score = 0
for character in invalid_characters:
    score += error_scoring[character]

print score
