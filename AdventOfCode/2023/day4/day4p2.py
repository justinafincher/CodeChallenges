#!/usr/bin/python3
# scratch cards 
import sys
import math

def calculate_score(winners):
    if winners < 1:
        return 0
    else:
        return 2**(winners-1)

input_file=open(sys.argv[1], "r")

card_tracker = {}

for input_value in input_file:
    input_value = input_value.strip()
    card_values = input_value.split(":")
    card_id = card_values[0].split()[1]
    #print(game_id)
    
    winning_numbers = card_values[1].split("|")[0].split()
    play_numbers = card_values[1].split("|")[1].split()

    print(winning_numbers)
    print(play_numbers)

    win_match_count = 0
    for number in play_numbers:
        if number in winning_numbers:
            win_match_count += 1
   
    print(f"{win_match_count} winners")
    if card_id in card_tracker.keys():
        card_tracker[card_id] += 1
    else:
        card_tracker[card_id] = 1

    initial_card_count = card_tracker[card_id]

    for i in range(int(card_id)+1, int(card_id)+win_match_count+1):
        print("Adding duplicate card "+str(i))
        if str(i) in card_tracker.keys():
            card_tracker[str(i)] += 1*initial_card_count
        else:
            card_tracker[str(i)] = 1*initial_card_count

    print(card_tracker)
    print(" ")
    #break

    

print(sum(card_tracker.values()))
