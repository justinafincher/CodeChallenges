#!/usr/bin/python
# track crab submarines with fuel usage 
import sys
import math
import numpy

def calculate_fuel(submarine_array, base_point):
    total_fuel = 0
    for value in submarine_array:
        base_difference = abs(base_point - value)
        fuel_usage = (0.5*(base_difference**2)) + (0.5*base_difference)
        total_fuel += fuel_usage
    return total_fuel

input_file=open(sys.argv[1], "r")

submarine_positions = []
# just leaving the loop even though this one is a one line input
for input_value in input_file:
    input_value_line = input_value.strip()
    input_values = input_value.split(",")
    for value in input_values:
        submarine_positions.append(int(value))
    
min_fuel = -1
for i in range(min(submarine_positions), max(submarine_positions)):
    fuel_usage = calculate_fuel(submarine_positions, i)
    if min_fuel == -1 or fuel_usage < min_fuel:
        min_fuel = fuel_usage

print "Minimum fuel: " + str(min_fuel)

