#!/usr/bin/python
# Check passports for required fields

import sys
import math
import re

input_file=open(sys.argv[1], "r")

required_fields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]
valid_eye_colors = ["amb","blu","brn","gry","grn","hzl","oth"]
hair_regex = re.compile('#[0-9a-f]{6}$', re.I)
pid_regex = re.compile('[0-9]{9}$', re.I)

def validate_passport(passport):
    temp_fields = passport.split(" ")
    
    for field in temp_fields:
        field_pieces = field.split(":")
        field_name = field_pieces[0]
        if field_name in required_fields:
            temp_field_check[field_name] = field_pieces[1]
    if len(temp_field_check) == 7:
        # break out fields for simplicity
        byr = temp_field_check["byr"]
        iyr = temp_field_check["iyr"]
        eyr = temp_field_check["eyr"]
        hgt = temp_field_check["hgt"][:-2]
        hgt_units = temp_field_check["hgt"][-2:]
        hcl = temp_field_check["hcl"]
        ecl = temp_field_check["ecl"]
        pid = temp_field_check["pid"]

        # Validate birth year
        if int(byr) < 1920 or int(byr) > 2002:
            print "Invalid birth year: " + byr
            return 0
        # Validate issue year
        if int(iyr) < 2010 or int(iyr) > 2020:
            print "Invalid issue year: " + iyr
            return 0
        # Validate expiration year
        if int(eyr) < 2020 or int(eyr) > 2030:
            print "Invalid expiration year: " + eyr
            return 0
        # Validate height
        if hgt_units == "in":
            if int(hgt) < 59 or int(hgt) > 76:
                print "Invalid height in inches: " + hgt
                return 0
        else:
            if int(hgt) < 150 or int(hgt) > 193:
                print "Invalid height in cm:" + hgt
                return 0
        # Validate hair color
        if not hair_regex.match(str(hcl)):
            print "Invalid hair color: " + hcl
            return 0
        # Validate eye color
        if ecl not in valid_eye_colors:
            print "Invalid eye color " + ecl
            return 0
        # Validate passport ID
        if not pid_regex.match(str(pid)):
            print "Invalid passport id: " + pid 
            return 0
        #print "Found valid passport : " + temp_passport
        return 1
    else:
        return 0

valid_passports = 0

temp_passport = ""
temp_field_check = {}
for input_value in input_file:
    input_value = input_value.strip()
    if len(input_value) == 0:
        print "end of passport, processing:" + temp_passport
        valid_passports += validate_passport(temp_passport)
        
        temp_field_check = {}
        temp_passport = ""
    else:
        temp_passport = temp_passport + " " + input_value

# oops, forgot the last one
if len(temp_passport) != 0:
    print "end of passport, processing:" + temp_passport
    valid_passports += validate_passport(temp_passport)

print valid_passports
