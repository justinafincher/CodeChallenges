use strict;
use warnings;
use Data::Dumper;

my $filename = 'day3input';
#$filename = 'day3testinput2';

open(my $fh, '<:encoding(UTF-8)', $filename)
  or die "Could not open file '$filename' $!";

my $triangles = 0;

my $rows_stored = 0;
my @first_row;
my @second_row;
my @third_row;

my $counter = 0;

while (my $row = <$fh>) {
   chomp $row;
   
   my @first_row = split(' ',$row);
   my $row = <$fh>;
   my @second_row = split(' ',$row);
   $row = <$fh>;
   my @third_row = split(' ',$row);

   for(my $i = 0; $i <= 2; $i++){
      my @current_triangle;
      my $min;
      my $mid;
      my $max;
      
      push(@current_triangle,$first_row[$i]);
      push(@current_triangle,$second_row[$i]);
      push(@current_triangle,$third_row[$i]);
  
      print Dumper(@current_triangle); 
      foreach my $value (@current_triangle) {
         if($value == find_min(@current_triangle) && !defined($min)){
            $min = $value;
         }
         elsif($value == find_max(@current_triangle) && !defined($max)){
            $max = $value;
         }
         else{
            $mid = $value;
         }
      }

      if(!defined($min) || !defined($max) || !defined($mid)){
         print "Error, missing value: $min|$mid|$max for $row\n";
      }

      if(($min+$mid) > $max){
         $triangles++;
         print "POSSIBLE: $min|$mid|$max\n";
      }
      else{
         print "NOT POSSIBLE: $min|$mid|$max\n";
      }
   }
   $counter++;
   #last if $counter > 2;
}

print "Found $triangles triangles\n";

sub find_min {
   my @values = @_;

   my $min = 10000000;
   foreach my $value (@values) {
      if($value < $min){
         $min = $value;
      }
   }
   return $min;
}

sub find_max {
   my @values = @_;

   my $max = -10000000;
   foreach my $value (@values) {
      if($value > $max){
         $max = $value;
      }
   }
   return $max;
}
