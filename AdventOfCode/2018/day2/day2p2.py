#!/usr/bin/python
# Read file with signed int on each line. Calculate sum.

input_file=open("day2input.txt", "r")

all_strings = []

for line in input_file:
    all_strings.append(line.strip('\n'))

for line in all_strings:
    for other_line in all_strings:
        # compare strings to find number of differences
