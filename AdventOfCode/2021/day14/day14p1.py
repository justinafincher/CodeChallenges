#!/usr/bin/python
# process polymer insertions 
import sys
import math
import numpy

input_file=open(sys.argv[1], "r")

def do_insertions(initial_string, instructions):
    new_string = ""
    for i in range(1, len(initial_string)):
        new_string += initial_string[i-1]
        if initial_string[i-1:i+1] in instructions:
            new_string += instructions[initial_string[i-1:i+1]]
    new_string += initial_string[i]
    
    return new_string

def calculate_score(input_string):
    characters = set(input_string)
    character_counts = {}
    for character in characters:
        character_counts[character] = input_string.count(character)

    return max(character_counts.values()) - min(character_counts.values())

current_string = ""
instructions = {}

for input_value in input_file:
    input_value = input_value.strip()
    if(len(input_value) == 0):
        next
    elif "->" not in input_value:
        current_string = input_value
    else:
        instructions[input_value.split(" -> ")[0]] = input_value.split(" -> ")[1]


iterations = 10

for i in range(0, iterations):
    new_string = do_insertions(current_string, instructions)
    current_string = new_string
    print "length: " + str(len(current_string))

print calculate_score(current_string)
