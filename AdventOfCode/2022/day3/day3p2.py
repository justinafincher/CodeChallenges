#!/usr/bin/python3

import sys
import math
from difflib import SequenceMatcher

input_file=open(sys.argv[1], "r")

total_score = 0

# Find common character in groups of 3
group_counter = 0
group_dict = {}

for input_value in input_file:
    temp_dict = {}
    for char_i in range(0, len(input_value.strip())):
        if input_value[char_i] not in temp_dict.keys():
            temp_dict[input_value[char_i]] = 1

    for key in temp_dict.keys():
        if key not in group_dict.keys():
            group_dict[key] = 1
        else:
            group_dict[key] += 1

    group_counter += 1

    if group_counter > 2:
        for key in group_dict.keys():
            if group_dict[key] == 3:
                if ord(key) <= 90:
                    total_score += ord(key)-38
                else:
                    total_score += ord(key)-96
        
        group_counter = 0
        group_dict = {}
#print("Total score: "+str(total_score))
#elf_calories.append(temp_elf)

print(total_score)
#print(max(elf_calories))
