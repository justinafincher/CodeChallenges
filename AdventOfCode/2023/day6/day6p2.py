#!/usr/bin/python3
# Calculate number of ways to win boat races 
import sys
import math

def calculate_distance(charge_time, total_time):
    return (int(total_time)-int(charge_time))*int(charge_time)

input_file=open(sys.argv[1], "r")

ways_to_win = 0

time = 0
distance = 0

for input_value in input_file:
    input_value = input_value.strip()
    input_values = input_value.split()
    if input_values[0] == "Time:":
        time = int("".join(input_value.split()[1:]))
    elif input_values[0] == "Distance:":
        distance = int("".join(input_value.split()[1:]))
    else:
        print("Unexpected input: "+input_value)

print(time)
print(distance)

# Let's try brute force
for j in range(0, time+1):
    if distance < calculate_distance(j, time):
        ways_to_win += 1

print(ways_to_win)
