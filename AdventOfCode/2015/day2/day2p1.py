#!/usr/bin/python3
# calculate needed wrapping paper
from functools import reduce
from operator import mul
import sys
import math

input_file=open(sys.argv[1], "r")

area = 0

def calculate_paper_needed(dimensions):
    if len(dimensions) != 3:
        print("Wrong dimensions!")
        exit
    sides = [int(dimensions[0])*int(dimensions[1]), int(dimensions[0])*int(dimensions[2]), int(dimensions[1])*int(dimensions[2])]
    total_area = 2*sum(sides) + min(sides)
    return total_area


for input_value in input_file:
    input_value = input_value.strip()
    input_values = input_value.split("x")
    area += calculate_paper_needed(input_values)

print(area)

