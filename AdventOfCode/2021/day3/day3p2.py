#!/usr/bin/python
# Process binary diagnostic report. 
import sys
import math

input_file=open(sys.argv[1], "r")

total_input = []

# Take a set of inputs, return relevant rows based on index.
# For oxygen, ties keep 1s
def oxygen_filter_rows_wrapper(input_values):
    print input_values
    for i in range(0, len(input_values[0])):
        input_values = oxygen_filter_rows(input_values, i)
        print input_values
        if len(input_values) == 1:
            break
    return input_values

def oxygen_filter_rows(input_values, column_index):
    # generate overall counts
    total_count = 0
    counts = []
    
    for input_value in input_values:
        if len(counts) != len(input_value):
            for char in input_value:
                counts.append(0)
                
        for i in range(0, len(input_value)):
          counts[i] += int(input_value[i])

        total_count += 1
        
    filter_string = ""

    for value in counts:
        if value >= (total_count / 2.0):
            #print "Adding 1 because " + str(value) + " >= " + str(total_count / 2.0)
            filter_string += "1"
        else:
            #print "Adding 0"
            filter_string += "0"

    # now that we have a basic filter string, we can build a new set of rows filtered down
    #print "filter string: " + filter_string
    new_values = []
    for input_value in input_values:
        if input_value[column_index] == filter_string[column_index]:
            new_values.append(input_value)
    
    return new_values

# Take a set of inputs, return relevant rows based on index.
# For co2, ties keep 0s
def co2_filter_rows_wrapper(input_values):
    print input_values
    for i in range(0, len(input_values[0])):
        input_values2 = co2_filter_rows(input_values, i)
        print "Checking index " + str(i)
        print input_values2
        if len(input_values2) == 1:
            break
        if len(input_values2) > 0:
            input_values = input_values2
    
    return input_values

def co2_filter_rows(input_values, column_index):
    # generate overall counts
    total_count = 0
    counts = []
    
    for input_value in input_values:
        if len(counts) != len(input_value):
            for char in input_value:
                counts.append(0)
                
        for i in range(0, len(input_value)):
          counts[i] += int(input_value[i])

        total_count += 1
        
    filter_string = ""

    for value in counts:
        # ugle edge case handle
        if value == 1 and total_count == 2:
            print "Adding 1 for edge case"
            filter_string += "1"
        elif value > (total_count / 2.0):
            print "Adding 1 because " + str(value) + " >= " + str(total_count / 2.0)
            filter_string += "1"
        else:
            print "Adding 0"
            filter_string += "0"

    print filter_string
    # now that we have a basic filter string, we can build a new set of rows filtered down
    new_values = []
    for input_value in input_values:
        if input_value[column_index] != filter_string[column_index]:
            new_values.append(input_value)
    
    return new_values

# we'll do a couple passes, so just store the input
for input_value in input_file:
    input_value = input_value.strip()
    total_input.append(input_value)

# Oxygen generator rating
# Determine most common value in position 1, keep only rows with that value
print "Calculating oxygen values"
oxygen_values = oxygen_filter_rows_wrapper(total_input)
print "Calculating co2 values"
co2_values = co2_filter_rows_wrapper(total_input)
#print total_input
print "Final values"
print oxygen_values
print co2_values
print "Result: " + str(int(oxygen_values[0], 2) * int(co2_values[0], 2))
