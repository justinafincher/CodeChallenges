#!/usr/bin/python
# Find program value before infinite loop 

import sys
import math
import re

input_file=open(sys.argv[1], "r")

instructions = []

jmp_nop = []
i = 0
for input_value in input_file:
    input_value = input_value.strip()
    instructions.append(input_value)
    # Let's log nop and jmp instructions to facilitate the switching
    if input_value.split(' ')[0] == "jmp" or input_value.split(' ')[0] == "nop":
        jmp_nop.append(i)
    i += 1

found_switch = 0
for index_to_switch in jmp_nop:
    instruction_tracker = {}
    accumulator = 0
    instruction_index = 0
    if found_switch == 1:
        break
    while 1:
        # attempting to move past the end of the program is a successful exit
        if instruction_index >= len(instructions):
            print accumulator
            found_switch = 1
            break
   
        input_values = instructions[instruction_index].split(' ')
        instruction = input_values[0]
        argument = int(input_values[1])

        # Check if we need to switch the instruction
        if instruction_index == index_to_switch:
            if instruction == "nop":
                instruction = "jmp"
            elif instruction == "jmp":
                instruction = "nop"
            else:
                print "OOPS, got the index wrong"


        # Exit if trying to repeat an instruction
        if instruction_tracker.has_key(instruction_index):
            break

        instruction_tracker[instruction_index] = 1
        
        if instruction == "nop":
            instruction_index += 1
        if instruction == "acc":
            accumulator += argument
            instruction_index += 1
        if instruction == "jmp":
            instruction_index += argument
