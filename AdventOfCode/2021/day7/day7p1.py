#!/usr/bin/python
# track crab submarines with fuel usage 
import sys
import math
import numpy

input_file=open(sys.argv[1], "r")

submarine_positions = []
total_fuel = 0
# just leaving the loop even though this one is a one line input
for input_value in input_file:
    input_value_line = input_value.strip()
    input_values = input_value.split(",")
    for value in input_values:
        submarine_positions.append(int(value))
    median_value = numpy.median(submarine_positions)
    for value in submarine_positions:
        total_fuel += abs(median_value - value)

print "Total fuel: " + str(total_fuel)

