use strict;
use warnings;
use Data::Dumper;

my $filename = 'day3input';
#$filename = 'day3testinput';

open(my $fh, '<:encoding(UTF-8)', $filename)
  or die "Could not open file '$filename' $!";

my $triangles = 0;

while (my $row = <$fh>) {
   chomp $row;
   
   my @values = split(' ',$row);
   my $min;
   my $mid;
   my $max;

   foreach my $value (@values) {
      if($value == find_min(@values) && !defined($min)){
         $min = $value;
      }
      elsif($value == find_max(@values) && !defined($max)){
         $max = $value;
      }
      else{
         $mid = $value;
      }
   }

   if(!defined($min) || !defined($max) || !defined($mid)){
      print "Error, missing value: $min|$mid|$max for $row\n";
   }

   if(($min+$mid) > $max){
      $triangles++;
      print "POSSIBLE: $min|$mid|$max\n";
   }
   else{
      print "NOT POSSIBLE: $min|$mid|$max\n";
   }
}

print "Found $triangles triangles\n";

sub find_min {
   my @values = @_;

   my $min = 10000000;
   foreach my $value (@values) {
      if($value < $min){
         $min = $value;
      }
   }
   return $min;
}

sub find_max {
   my @values = @_;

   my $max = -10000000;
   foreach my $value (@values) {
      if($value > $max){
         $max = $value;
      }
   }
   return $max;
}
