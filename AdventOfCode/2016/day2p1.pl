use strict;
use warnings;
use Data::Dumper;

my $filename = 'day2input';
#$filename = 'day2testinput';

my $xcoord = 0;
my $ycoord = 0;
my %lookup = (
   "-1,1" => 1,
   "0,1" => 2,
   "1,1" => 3,
   "-1,0" => 4,
   "0,0" => 5,
   "1,0" => 6,
   "-1,-1" => 7,
   "0,-1" => 8,
   "1,-1" => 9
);

my $passcode = "";

open(my $fh, '<:encoding(UTF-8)', $filename)
  or die "Could not open file '$filename' $!";
   
while (my $row = <$fh>) {
   chomp $row;
   
   my @directions = split('',$row);
   foreach my $direction (@directions) {
      if($direction eq "U" && $ycoord != 1){
         $ycoord++;
      }
      elsif($direction eq "D" && $ycoord != -1){
         $ycoord--;
      }
      elsif($direction eq "L" && $xcoord != -1){
         $xcoord--;
      }
      elsif($direction eq "R" && $xcoord != 1){
         $xcoord++;
      }
      #else{
      #   print "EXCEPTION: Direction unknown ($direction)\n";
      #   exit;
      #}
   }

   $passcode .= $lookup{"$xcoord,$ycoord"};
}

print "Passcode is $passcode\n";

