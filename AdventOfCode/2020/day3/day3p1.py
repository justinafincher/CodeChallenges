#!/usr/bin/python
# Given map of trees, you have a sled that traverses by going down 1 and over 3. Given this
# trajectory, how many trees will you hit by the time you hit the bottom?
import sys
import math
import re

input_file=open(sys.argv[1], "r")

trees_hit = 0
x_val = 0

# Each line in form . for open and # for tree
for input_value in input_file:
    input_value = input_value.strip()
    index_to_check = x_val % len(input_value)
    
    if input_value[index_to_check] == "#":
        trees_hit += 1
    
    x_val = index_to_check + 3 
    
print trees_hit
