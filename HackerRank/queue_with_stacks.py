#!/usr/bin/python

class MyQueue(object):
    def __init__(self):
       self.first = [] # the stack of unprocessed values
       self.second = [] # holder for ready to be processed values

    def peek(self):
       if len(self.second) == 0:
          self.copyToStack2()

       return self.second[len(self.second)-1]

    def pop(self):
       if len(self.second) == 0:
          self.copyToStack2()
       
       self.second.pop()
        
    def put(self, value):
       self.first.append(value) 

    def copyToStack2(self):
       for i in range(0,len(self.first)):
          self.second.append(self.first.pop())

queue = MyQueue()
t = int(raw_input())
for line in xrange(t):
    values = map(int, raw_input().split())
    
    if values[0] == 1:
        queue.put(values[1])        
    elif values[0] == 2:
        queue.pop()
    else:
        print queue.peek()
