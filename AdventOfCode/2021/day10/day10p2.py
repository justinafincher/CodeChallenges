#!/usr/bin/python
# Process corrupted navigational input 
import sys
import math
import numpy

completion_scoring = {
    "(":1,
    "[":2,
    "{":3,
    "<":4
}

input_file=open(sys.argv[1], "r")

chunk_definitions = {
    "(":")",
    "[":"]",
    "{":"}",
    "<":">"
}

incomplete_lines = []
checked_incomplete_lines = []


for input_value in input_file:
    is_invalid = 0
    input_value_line = input_value.strip()
    checking_array = []
    for i in range(0, len(input_value_line)):
        if input_value_line[i] in chunk_definitions.keys():
            checking_array.append(input_value_line[i])
        elif input_value_line[i] in chunk_definitions.values():
            if chunk_definitions[checking_array[-1]] == input_value_line[i]:
                checking_array.pop()
            else:
                #print "Invalid character: "+input_value_line[i]
                is_invalid = 1
                break
        else:
            print "Unknown character found: "+input_value_line[i]

    if is_invalid == 0:
        checked_incomplete_lines.append("".join(checking_array[::-1]))

scores = []
for string in checked_incomplete_lines:
    temp_score = 0
    for character in string:    
        temp_score *= 5
        temp_score += completion_scoring[character]
    scores.append(temp_score)

scores.sort()
print scores[len(scores)/2]
