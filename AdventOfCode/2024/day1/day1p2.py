#!/usr/bin/python3
# With list of pairs of numbers, starting with smallest numbers and add up differences 
import sys
import math

input_file=open(sys.argv[1], "r")

left_list = []
right_list = {}
similarity_score = 0


for input_value in input_file:
    input_value = input_value.strip()
    input_values = input_value.split()

    left_list.append(input_values[0])
    if input_values[1] in right_list.keys():
        right_list[input_values[1]] += 1
    else:
        right_list[input_values[1]] = 1

for i in range(0,len(left_list)):
    if left_list[i] in right_list.keys():
        similarity_score += int(left_list[i]) * right_list[left_list[i]]

print(f"Similarity score: {similarity_score}")
