#!/usr/bin/python3
# read calibration values, combine first and last digits, then sum
import sys
import math

number_replacements = {
    "one" : "1",
    "two": "2",
    "three": "3",
    "four" : "4",
    "five" : "5",
    "six" : "6",
    "seven" : "7",
    "eight" : "8",
    "nine" : "9"
}

input_file=open(sys.argv[1], "r")

calibrations = []

for input_value in input_file:
    input_value = input_value.strip()
   
    print(input_value)
    #rebuilding_string = input_value[0:2]
    #for i in range(2, len(input_value)):
    #    rebuilding_string += input_value[i]
    #    print("rebuilding_string: "+rebuilding_string)
    #    for num_string, number in number_replacements.items():
    #        rebuilding_string = rebuilding_string.replace(num_string.lower(), number)
    #print(rebuilding_string)

    #temp_string = ""
    #for character in rebuilding_string:
    #    if character.isnumeric():
    #        temp_string += character
   
    # find first value
    first_value = -1
    for char in input_value[0:2]:
        if char.isnumeric():
            first_value = char
            break
    if first_value == -1:
        rebuilding_string = input_value[0:2]
        for i in range(2, len(input_value)):
            if input_value[i].isnumeric():
                first_value = input_value[i]
                break
            rebuilding_string += input_value[i]
            for num_string, number in number_replacements.items():
                if rebuilding_string.find(num_string) != -1:
                    first_value = number
                    break
            if int(first_value) > -1:
                break
    print(first_value + " is the first number in the string")

    # find second value
    second_value = -1
    for char in input_value[max(len(input_value)-3,0):len(input_value)]:
        print("checking initial char: "+char)
        if char.isnumeric():
            second_value = char
    if second_value == -1:
        rebuilding_string = input_value[len(input_value)-3:len(input_value)]
        print("initial rebuilding string: " + rebuilding_string)
        for i in range(len(input_value)-4, -1, -1):
            for num_string, number in number_replacements.items():
                if rebuilding_string.find(num_string) != -1:
                    second_value = number
                    break
            if int(second_value) > -1:
                break
            print("checking for numeric of "+input_value[i])
            if input_value[i].isnumeric():
                second_value = input_value[i]
                break
            rebuilding_string = input_value[i] + rebuilding_string
            print(rebuilding_string)
    print(str(second_value) + " is the last number in the string")
    #print(temp_string)
    #print(temp_string[0]+temp_string[-1])
    print("")
    calibrations.append(int(str(first_value)+str(second_value)))

print(sum(calibrations))
