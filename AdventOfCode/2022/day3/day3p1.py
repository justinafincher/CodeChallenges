#!/usr/bin/python3
# Given rock paper scissors guide, calculate score 
import sys
import math
from difflib import SequenceMatcher

input_file=open(sys.argv[1], "r")

total_score = 0

for input_value in input_file:
    string1 = input_value.strip()[0:math.floor(len(input_value)/2)]
    string2 = input_value.strip()[math.floor(len(input_value)/2):]

    print(string1)
    print(string2)

    match_char = string1[SequenceMatcher(None, string1, string2).find_longest_match()[0]]

    
    print(match_char)

    if ord(match_char) <= 90:
        total_score += ord(match_char)-38
    else:
        total_score += ord(match_char)-96

#print("Total score: "+str(total_score))
#elf_calories.append(temp_elf)

print(total_score)
#print(max(elf_calories))
