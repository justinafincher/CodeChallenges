#!/usr/bin/python
# Navigate hyperthermal vents
import sys
import math
import numpy

def count_overlaps():
    count = 0
    #for i in range(0, 10):
    #    for j in range(0, 10):
    for i in range(0, 1000):
        for j in range(0, 1000):
            if floor_map[i,j] > 1:
                count += 1
    return count

input_file=open(sys.argv[1], "r")

#floor_map = numpy.zeros((10, 10)) 
floor_map = numpy.zeros((1000, 1000)) 

for input_value in input_file:
    input_value = input_value.strip()
    input_values = input_value.split(" -> ")
    first_coord = input_values[0].split(",")
    second_coord = input_values[1].split(",")

    print input_values
    if first_coord[0] != second_coord[0] and first_coord[1] != second_coord[1]:
        # Since we only have 45 degree diagonals, the difference between x or y coords is the number of steps (plus 1 since it is inclusive) 
        num_diagonals = abs(int(first_coord[0]) - int(second_coord[0])) + 1
        # diagonal top-left to bottom-right means both second coords are greater than first coords
        if int(first_coord[0]) < int(second_coord[0]) and int(first_coord[1]) < int(second_coord[1]):
            print "Processing less than/less than"
            for i in range(0, num_diagonals):
                floor_map[int(first_coord[1])+i, int(first_coord[0])+i] += 1
                print "incrementing diagonal " + str(int(first_coord[0])+i) + "," + str( int(first_coord[1])+i)
        # diagonal top-right to bottom-left means first first coord is less than first second coord and second first coord is less than second second coord
        elif int(first_coord[0]) < int(second_coord[0]) and int(first_coord[1]) > int(second_coord[1]):
            print "Processing less than/greater than"
            for i in range(0, num_diagonals):
                floor_map[int(first_coord[1])-i, int(first_coord[0])+i] += 1
                print "incrementing diagonal " + str(int(first_coord[0])+i) + "," + str( int(first_coord[1])-i)
        # diagnoal bottom-left to top-right means first first coord is greater than first second coord and second first coord is less than second second coord
        elif int(first_coord[0]) > int(second_coord[0]) and int(first_coord[1]) < int(second_coord[1]):
            print "Processing greater than/less than"
            for i in range(0, num_diagonals):
                floor_map[int(first_coord[1])+i, int(first_coord[0])-i] += 1
                print "incrementing diagonal " + str(int(first_coord[0])-i) + "," + str( int(first_coord[1])+i)
        # diagnoal bottom-right to top-left means first first coord is greater than first second coord and second first coord is greater than second second coord
        else:
            print "Processing greater than/greater than"
            for i in range(0, num_diagonals):
                floor_map[int(first_coord[1])-i, int(first_coord[0])-i] += 1
                print "incrementing diagonal " + str(int(first_coord[0])-i) + "," + str( int(first_coord[1])-i)
    elif first_coord[0] != second_coord[0]:
        for i in range(min(int(first_coord[0]), int(second_coord[0])), (max(int(first_coord[0]), int(second_coord[0]))+1)):
            floor_map[int(first_coord[1]), i] += 1
    elif first_coord[1] != second_coord[1]:
        for i in range(min(int(first_coord[1]), int(second_coord[1])), (max(int(first_coord[1]), int(second_coord[1]))+1)):
            floor_map[i, int(first_coord[0])] += 1

    print floor_map

print count_overlaps()
