#!/usr/bin/python
# after preamble of 25 numbers, each number must be a sum of 2 of the previous 25 numbers 
import sys
import math

input_file=open(sys.argv[1], "r")

preamble_length = 25
values = []

for input_value in input_file:
    input_value = input_value.strip()
    values.append(input_value)

for check_index in range(preamble_length, len(values)):
    value_to_validate = int(values[check_index])
    found_sum = 0
    for i in range(check_index - preamble_length, check_index):
        for j in range(check_index - preamble_length, check_index):
            if i == j:
                continue
            if int(values[i]) + int(values[j]) == value_to_validate:
                found_sum = 1

    if found_sum == 0:
        print "No values for sum found for " + str(value_to_validate)
        break
