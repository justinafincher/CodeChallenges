#!/usr/bin/python
# Read file with int on each line. Divide by 3, round down, then subtract 2.
import sys
import math

def truncate(n, decimals=0):
    multiplier = 10 ** decimals
    return int(n * multiplier) / multiplier

input_file=open(sys.argv[1], "r")

total_fuel_requirement = 0

for input_value in input_file:
    fuel_requirement = truncate(int(input_value)/3,0)-2
    total_fuel_requirement += fuel_requirement

print "Total fuel requirement: "+str(total_fuel_requirement)
