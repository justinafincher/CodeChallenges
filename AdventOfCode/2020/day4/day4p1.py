#!/usr/bin/python
# Check passports for required fields

import sys
import math
import re

input_file=open(sys.argv[1], "r")

required_fields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]

valid_passports = 0

temp_passport = ""
temp_field_check = {}
for input_value in input_file:
    input_value = input_value.strip()
    if len(input_value) == 0:
        print "end of passport, processing:" + temp_passport
        temp_fields = temp_passport.split(" ")
        
        for field in temp_fields:
            #print "Processing field |" + field + "|"
            field_pieces = field.split(":")
            field_name = field_pieces[0]
            if field_name in required_fields:
                temp_field_check[field_name] = field_pieces[1]
            #else:
            #    print field_pieces[0] + " not found in required fields"
        if len(temp_field_check) == 7:
            print "Found valid passport : " + temp_passport
            valid_passports += 1
        else:
            print "Passport failed check"
            print temp_field_check
        temp_field_check = {}
        temp_passport = ""
        continue

    temp_passport = temp_passport + " " + input_value

# oops, forgot the last one
if len(temp_passport) != 0:
    print "end of passport, processing:" + temp_passport
    temp_fields = temp_passport.split(" ")
    
    for field in temp_fields:
        field_pieces = field.split(":")
        field_name = field_pieces[0]
        if field_name in required_fields:
            temp_field_check[field_name] = field_pieces[1]
    if len(temp_field_check) == 7:
        print "Found valid passport : " + temp_passport
        valid_passports += 1
    else:
        print "Passport failed check"
        print temp_field_check

print valid_passports
