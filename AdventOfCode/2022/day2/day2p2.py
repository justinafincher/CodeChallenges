#!/usr/bin/python3
# Given rock paper scissors guide, calculate score 
import sys
import math

input_file=open(sys.argv[1], "r")

# values for each play choice
play_scores = {
    "X":1,
    "Y":2,
    "Z":3
}

beat_by = {
    "A":"Y",
    "B":"Z",
    "C":"X"
}

beats = {
    "A":"Z",
    "B":"X",
    "C":"Y"
}

play_translation = {
    "A":"X",
    "B":"Y",
    "C":"Z"
}

desired_outcome_scores = {
    "X": 0, # lose
    "Y": 3, # draw
    "Z": 6  # win
}


total_score = 0

for input_value in input_file:
    round_score = 0
    values = input_value.strip().split(" ")
    round_score += desired_outcome_scores[values[1]]

    if values[1] == "Z":
        round_score += play_scores[beat_by[values[0]]]
    elif values[1] == "Y":
        round_score += play_scores[play_translation[values[0]]]
    else:
        round_score += play_scores[beats[values[0]]]

    total_score += round_score

print("Total score: "+str(total_score))
#elf_calories.append(temp_elf)

#print(elf_calories)
#print(max(elf_calories))
