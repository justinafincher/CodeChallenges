#!/usr/bin/python3
# Given corrupted memory, identify multiply operations and sum results 
import sys
import re

input_file=open(sys.argv[1], "r")

result_sum = 0


def calculate_multiply(mult_string):
    mult_string = mult_string.strip('mul(')
    mult_string = mult_string.rstrip(')')
    values = mult_string.split(',')
    
    return int(values[0]) * int(values[1])

input_string = ""

for input_value in input_file:
    input_string += input_value.strip()


print(f"Input: {input_string}")

# Start with everything before the first "don't" since we start with "do"
#initial_regex = re.compile(r'^(.*?)(?:don\'t\(\))')
everything_else_regex = re.compile('do(?!n\'t\(\))(.*?)(?=don\'t\(\)|$)')

multiply_pattern = 'mul\([0-9]+,[0-9]+\)'

# Combine all valid strings
split_values = input_string.split("don't")
cleaned_input = split_values[0]

#initial_search = re.search(initial_regex, input_value)

#if initial_search:
#    cleaned_input += initial_search.group(0)

print(f"Cleaned input after initial search: {cleaned_input}")

if len(split_values) > 1:
    remaining_values = "don't"+"don't".join(split_values[1:]) 
    #print(f"Remaining values: {remaining_values}")
    for search_result in re.findall(everything_else_regex, remaining_values):
        #print("----")
        #print(search_result)
        #print("----")
        cleaned_input += search_result
    #print("end everything else search")

#cleaned_input = ""
#enabled = True
#for i in range(0, len(input_value)):
#    if i > 7 and input_value[i-7:i] == "don't()":
#        enabled = False
#    elif i > 4 and input_value[i-4:i] == "do()":
#        enabled = True
#    
#    if enabled:
#        cleaned_input += input_value[i]

print(f"Cleaned input: {cleaned_input}")

multiplies = re.findall(multiply_pattern, cleaned_input)

for multiply in multiplies:
    print(f"Calculating result for {multiply}")
    
    result_sum += calculate_multiply(multiply)

print(result_sum)
