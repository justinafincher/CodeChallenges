#!/usr/bin/python
# after preamble of 25 numbers, each number must be a sum of 2 of the previous 25 numbers 
import sys
import math

input_file=open(sys.argv[1], "r")

preamble_length = 25
values = []
invalid_value = -1

for input_value in input_file:
    input_value = input_value.strip()
    values.append(input_value)

for check_index in range(preamble_length, len(values)):
    value_to_validate = int(values[check_index])
    found_sum = 0
    for i in range(check_index - preamble_length, check_index):
        for j in range(check_index - preamble_length, check_index):
            if i == j:
                continue
            if int(values[i]) + int(values[j]) == value_to_validate:
                found_sum = 1

    if found_sum == 0:
        print "No values for sum found for " + str(value_to_validate)
        invalid_value = value_to_validate
        break

# Now that we have the invalid value, find a set of contiguous numbers that add up to it
contiguous_numbers = []
print "Looking for numbers summing to " + str(invalid_value)
for value in values:
    contiguous_numbers.insert(0, int(value))
    print contiguous_numbers
    temp_sum = sum(contiguous_numbers)
    print temp_sum
    if temp_sum == invalid_value:
        print "FOUND SUM"
        print contiguous_numbers
        break
    elif temp_sum > invalid_value:
        while sum(contiguous_numbers) > invalid_value:
            contiguous_numbers.pop()
        if sum(contiguous_numbers) == invalid_value:
            print "FOUND SUM"
            print contiguous_numbers
            print min(contiguous_numbers) + max(contiguous_numbers)
            break
