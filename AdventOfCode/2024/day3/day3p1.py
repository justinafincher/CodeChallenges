#!/usr/bin/python3
# Given corrupted memory, identify multiply operations and sum results 
import sys
import re

input_file=open(sys.argv[1], "r")

result_sum = 0


def calculate_multiply(mult_string):
    mult_string = mult_string.strip('mul(')
    mult_string = mult_string.rstrip(')')
    values = mult_string.split(',')
    
    return int(values[0]) * int(values[1])


for input_value in input_file:
    input_value = input_value.strip()

    multiply_pattern = 'mul\([0-9]+,[0-9]+\)'
    
    multiplies = re.findall(multiply_pattern, input_value)

    for multiply in multiplies:
        print(f"Calculating result for {multiply}")
        
        result_sum += calculate_multiply(multiply)

print(result_sum)
