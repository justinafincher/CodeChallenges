#!/usr/bin/python3
# Using bash-like output, find directory sizes 
import sys
import math
import re

directory_size_limit = 100000
total_filesystem_size = 70000000
required_space = 30000000

def calculate_dir_sizes(file_list):
    directories = {}
    for file_name in file_list.keys():
        print(file_name)
        parent_directories = file_name.split("/")[:-1]
        print("processing directories")
        print(parent_directories)
        for i in range(1, len(parent_directories)+1):
            directory = "/".join(parent_directories[1:i])
            print("Processing directory: "+directory)
            if directory in directories.keys():
                directories[directory] += file_list[file_name]
            else:
                directories[directory] = file_list[file_name]
        print("Directories after processing "+file_name)
        print(directories)
    return directories


input_file=open(sys.argv[1], "r")

current_directory = "/"

# May be able to just track files with full paths, then use that to calculate directory sizes
files = {}

# Track when we do an ls
reading_files = False

for input_value in input_file:
    input_values = input_value.strip().split(" ")
    #print("NEW LINE ================")
    #print(input_values)
    #print(current_directory)

    if reading_files and input_values[0] == "$":
        reading_files = False

    # update current directory
    if input_values[1] == "cd":
        #print("Changing to directory "+input_values[2])
        if input_values[2] == "..":
            current_directory = "/".join(current_directory.split("/")[:-2])+"/"
        elif input_values[2] == "/":
            current_directory = "/"
        else:
            current_directory += (input_values[2]+"/")
    elif input_values[1] == "ls":
        reading_files = True
    else:
        if input_values[0] != "dir":
            files[current_directory+input_values[1]] = int(input_values[0])

print(files)
directory_list = calculate_dir_sizes(files)
print(directory_list)

# Now that we have the directory list, file the smallest directory that will free up enough space
space_to_free = required_space - (total_filesystem_size - int(directory_list[""]))
print("used space: "+str(directory_list[""]))
print("free space: "+str(total_filesystem_size - int(directory_list[""])))
print("space to free: "+str(space_to_free))

directory_to_delete_size = total_filesystem_size
directory_name = ""
for directory in directory_list.keys():
    if directory_list[directory] > space_to_free and directory_list[directory] < directory_to_delete_size:
        print("New smallest directory found")
        print(directory)
        print(directory_list[directory])
        directory_to_delete_size = directory_list[directory]
        directory_name = directory

print(directory_name)
print(directory_to_delete_size)
